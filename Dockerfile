FROM php:7-apache

RUN a2enmod rewrite
RUN apt-get update

RUN apt-get install -y libssl-dev apt-utils vim curl && \
    pecl install xdebug  && \
    pecl install mongodb && \
    docker-php-ext-enable mongodb

RUN echo 'ServerName localhost' >> /etc/apache2/apache2.conf

#change user to www-data (apache user) to have full access to app directory
RUN usermod -u 1000 www-data

# creates app folder
RUN mkdir -p /app/moviecatalog-restapi
RUN service apache2 restart
