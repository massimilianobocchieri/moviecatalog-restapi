MovieCatalogService REST API
========
Micro service exposing trough REST/HTTP API the features of a MovieCatalog

required by [CodeChallenge](https://hackmd.io/s/HJSzq7st-)
# Requirements

The following software dependencies are required to run this project, see the docker image for more clarification:

- PHP >= 7.1
- MongoDb
- Composer
- Docker Container (optional) 

# Installation

This project is using the PHP package manager `composer`:
- https://getcomposer.org/doc/01-basic-usage.md#installing-dependencies


To install the project dependencies, please run, under the project root folder:

 `composer install`

# Unit Test and coverage

From the project root run 

 1- Start Your Mongodb Deamon first 
 
 2- `php ./vendor/phpunit/phpunit/phpunit --configuration ./phpunit.xml --teamcity`
 
Coverage reports will be available in the  **build** folder


# Configuration File

Under the folder config you can find a file **app.json**

At the moment the key "db" 

=> "host" has "db" value (put in my /etc/hosts 127.0.0.1 db) 
=> "port" has "27017" value (mongodb default)

Please update them accordingly to your system configuration

 
# Service Start and Stop (Local Installation)

Start
    
From the project root run in Terminal

`php -S localhost:8080 -t ./public`


Rest Api will be available at 

[http://localhost:8080/api/movies](http://localhost:8080/api/movies)  (base URL)

[See API Specification Document](API.md) 

for REST invocations (methods, url, payload, format, HTTP statuscode).

 
Stop

1- `Press ^C` to Stop the PHP builtin WebServer
2- Stop MongoDb Deamon


# Docker

In the project folder there are

- DockerFile
- docker-compose.yml

Building essentially a container with PHP7-Apache, linked to a MongoDb one

To Use the service through Docker, from the project root run

`docker-compose up`

 
# Architecture considerations

I have decided to use frameworks at the minimum in order to mantain a clean architecture, not tangled by frameworks
structure. I used Slim from HTTP Request/Response for comodity, Monolog for Logging, Mongodb client library and an Enum
library implementation and Mockery for double/Mocking.

The source code of the project has been structured in two main branches (Infrastructure and Resources)

In the Infrastructure I have placed common usage classes and packages (Routing, Utility, etc...) sharable by Resources, 
that at the moment are only Movies.

I have used some pattern {Builder, Template Method} and used Interfaces as much as possible and Abstract Classes 
for common behaviour. I have used a Container to inject common services and dependencies.


One of the things I am proud if is having been able to defer DB persistence until the last phase of the project leaving
my business logic decoupled from the db "world" (You will find in a "src" sublfolder an InMemory implementation of the 
  Movie Repository interface)

# TODO

More unit tests to eliminate code fear, BDD (possibly Behat), further refactoring and possible reconsideration of the 
Exception Handling mechanism.










    