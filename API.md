API Specification for MovieCatalog Service Rest API application
=================

**I assume data format for request response is Json**

 **Show all movies**

    Request Type: GET
    Endpoint: /api/movies
    Status: 200
    Response: [Movie 1, Movie 2, ..., Movie n]  containing the list of Type Movie (even empty)
    
==     

 **Show Movies titles filtered by either {genre,director,star}**
 
    Request Type: POST
    Endpoint: /api/movies/title/searches
    Body : empty OR {"filterBy" : "genre", "filterValue" : "<a valid Movie genre>"} OR
    {"filterBy" : "director", "filterValue" : {"lastName" : "<director lastname>", "firstName" : "<director firstname>"}} OR
    {"filterBy" : "star", "filterValue" : {"lastName" : "<director lastname>", "firstName" : "<director firstname>"}}
    Status: 200 OR 416(Filter error)
    Response: [Movie Title1, Movie Title 2, ..., Movie Title n]  containing the list of Movie title (even empty) OR validation error info
Notes: With empty body will be returned allo movie titles

 **Show details of a movie with a given imdbid**

    Request Type: GET
    Endpoint: /api/movies/:imdbid (imdbid (from IMDB site) must have regexpr tt[0-9]{7})
    Response status: 200 or 404
    Response: {Movie} or {"info" : "movie <imdbid> not found"}

 **Delete a movie with a given imdbid**
 
    Request Type: DELETE
    Endpoint: /api/movies/:id
    Response status: 204(No Content) or 404
    Response: empty (in case of 204) or {"info" : "movie <imdbid> not found"}

 **Insert one or more movies**

    Request Type: POST
    Endpoint: /api/movies
    Body : Movie (in json format) i.e:
    {"imdbid": "tt1234566",
     "title": "The warrior",
     "genre" : "ACTION",
     "year" : 2017,
     "rating": 10,
     "directors" : [{"lastName" : "Malvestio", "firstName" : "Mauro"}],
     "stars": [{"lastName" : "Bocchieri", "firstName" : "Massimiliano"}]
    }
    
    OR [Movie1, Movie2, Movie N]+ (minumum 1 movie) for multiple inserts where Movie i has the above shown format
    
    Response status: 201 or 416 (filter error or movie/s already existing)
    Response: {Movie} | [Movie1,Movie2, .. , Movie n] with inserted movie/s OR ["validation error" : "<error details>"] OR ["error" => "the movie with imdbid <:imdbid> already exists, insertion aborted"] (Single movie)
    OR ["error" => "one or more movies already exist, insertion aborted"] (Single movie) 


 **Update info for a movie with a given imdbid**

    Request Type: PUT
    Endpoint: /api/movies/:imdbid
    Body : fields of a movie, in json format, not containing imdbid, because already passed in the resource url OR EMPTY
    
    Response status: 200 OR 416(validation error) or 404 (movie not found)
    Response: {Movie} with the updated values OR ["validation error" : "<error details>"] OR ["error" => "the movie with imdbid <:imdbid> not found, update aborted"]


**Movie Item Example**

    {"imdbid": "tt1234566",
     "title": "The warrior",
     "genre" : "ACTION",
     "year" : 2017,
     "rating": 10,
     "directors" : [{"lastName" : "Malvestio", "firstName" : "Mauro"}],
     "stars": [{"lastName" : "Bocchieri", "firstName" : "Massimiliano"}]
    }

