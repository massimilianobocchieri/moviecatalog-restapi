<?php
declare(strict_types=1);

namespace MovieCatalogRestApi;


use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\{
    ResponseInterface, ServerRequestInterface
};
use Slim\Http\Response;
use Exception;
use Throwable;

/**
 * Class App
 * @package MovieCatalogRestApi
 *
 * Main Application class
 *
 */
class App
{
    const VERSION = '1.0';

    private $container;
    private $logger;

    public function __construct(?ContainerInterface $container = null)
    {
        $this->container = $container ?? new DependencyInjectionContainer();
        $this->logger = $this->container->get('logger');
    }

    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }

    public function run(bool $isResponseRenderingSkipped = false): ?ResponseInterface
    {
        $request = $this->container->get('request');
        $response = $this->container->get('response');

        try {
            $response = $this->process($request, $response);
        } catch (Exception $ex) {
            $response = $this->handleException($ex, $request, $response);
        } catch (Throwable $throw) {
            $response = $this->handleError($throw, $request);
        }
        if (!$isResponseRenderingSkipped)
            $this->respond($response);

        return $response;
    }

    protected function process(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $router = $this->container->get('router');
        $response = $router->route($request);

        return $this->finalize($response);
    }

    private function finalize(ResponseInterface $response): ResponseInterface
    {
        ini_set('default_mimetype', '');

        if ($this->isEmptyResponse($response))
            return $response->withoutHeader('Content-Type')->withoutHeader('Content-Length');

        if (isset($this->container->get('settings')['addContentLengthHeader']) &&
            $this->container->get('settings')['addContentLengthHeader'] == true) {
            if (ob_get_length() > 0) {
                $message = "Unexpected data in output buffer. " .
                    "Maybe you have characters before an opening <?php tag?";
                $this->logger->error($message);
                throw new \RuntimeException($message);
            }

            $size = $response->getBody()->getSize();
            if ($size !== null && !$response->hasHeader('Content-Length'))
                $response = $response->withHeader('Content-Length', (string)$size);
        }

        return $response;
    }

    private function isEmptyResponse(ResponseInterface $response)
    {
        if (method_exists($response, 'isEmpty'))
            return $response->isEmpty();

        return in_array($response->getStatusCode(), [204, 205, 304]);
    }

    protected function handleException(Exception $ex, ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $this->logger->error(
            sprintf("Exception while processing the request %s %s: ",
                $request->getMethod(),
                $request->getUri()->getPath()),
            ['exception' => $ex]);

        return $response->withJson(["message" => "Error while processing you request"], 500);
    }

    protected function handleError(Throwable $throw, ServerRequestInterface $request): ResponseInterface
    {
        $this->logger->critical(
            sprintf("Error while processing the request %s %s:",
                $request->getMethod(),
                $request->getUri()->getPath()),
            ['exception' => $throw]);

        return (new Response())->withJson(["message" => "Internal Server Error while processing you request"], 500);
    }

    protected function respond(ResponseInterface $response): void
    {
        if (!$this->haveResponseHeadersBeenSent())
            $this->sendResponseHeaders($response);

        if (!$this->isEmptyResponse($response))
            $this->sendResponseBody($response);
    }

    private function haveResponseHeadersBeenSent(): bool
    {
        return headers_sent();
    }

    private function sendResponseHeaders(ResponseInterface $response): void
    {
        foreach ($response->getHeaders() as $name => $values) {
            foreach ($values as $value)
                header(sprintf('%s: %s', $name, $value), false);
        }

        header(sprintf(
            'HTTP/%s %s %s',
            $response->getProtocolVersion(),
            $response->getStatusCode(),
            $response->getReasonPhrase()
        ));
    }

    private function sendResponseBody(ResponseInterface $response): void
    {
        $body = $response->getBody();
        if ($body->isSeekable())
            $body->rewind();

        $settings = $this->container->get('settings');
        $chunkSize = $settings['responseChunkSize'];

        $contentLength = $response->getHeaderLine('Content-Length');
        if (!$contentLength)
            $contentLength = $body->getSize();

        if (isset($contentLength)) {
            $amountToRead = $contentLength;
            while ($amountToRead > 0 && !$body->eof()) {
                $data = $body->read(min($chunkSize, $amountToRead));
                echo $data;

                $amountToRead -= strlen($data);

                if (connection_status() != CONNECTION_NORMAL)
                    return;
            }
        } else {
            while (!$body->eof()) {
                echo $body->read($chunkSize);
                if (connection_status() != CONNECTION_NORMAL)
                    return;
            }
        }
    }

}