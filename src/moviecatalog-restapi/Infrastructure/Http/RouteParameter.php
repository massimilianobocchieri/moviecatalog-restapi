<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use InvalidArgumentException;

/**
 * Class RouteParameter
 *
 * Data structure used to map a route parameter
 *
 */
class RouteParameter
{
    const ALLOWED_PARAMETER_TYPES_AND_REGEXPR = ["NUMBER" => "\d+", "STRING" => "\w+", "ALPHANUMERIC" => "[\w\d]+", "IMDBID" => "tt[0-9]{7}"];

    private $name;
    private $type;

    public function __construct(string $name, string $type)
    {
        if (!$this->isTypeAllowed($type))
            throw new InvalidArgumentException("Route parameter type {$type} not allowed");

        $this->name = $name;
        $this->type = $type;
    }

    private function isTypeAllowed(string $typeToCheck): bool
    {
        return in_array($typeToCheck, array_keys(SELF::ALLOWED_PARAMETER_TYPES_AND_REGEXPR));
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getRegExpr(): ?string
    {
        return self::ALLOWED_PARAMETER_TYPES_AND_REGEXPR[$this->getType()];
    }

    public function getType(): string
    {
        return $this->type;
    }

}