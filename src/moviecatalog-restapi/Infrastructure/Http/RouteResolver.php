<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

/**
 * Class RouteResolver
 *
 * It tries to find the matching route, according to the request parameters
 */
class RouteResolver
{
    private $routes;

    public function __construct($routes)
    {
        $this->routes = $routes;
    }

    public function resolve(string $method, string $path): ?Route
    {
        foreach ($this->routes as $route)
            if ($route->matches($method, $path)) {
                $route->setExtractedParams($this->extractParams($route->getPath(), $path));
                return $route;
            }

        return null;
    }

    private function extractParams(string $routePath, string $path): array
    {
        $params = [];

        $pathParts = explode('/', substr($path, 1));
        $routeParts = explode('/', substr($routePath, 1));

        foreach ($routeParts as $key => $routePart) {
            if (strpos($routePart, ':') === 0) {
                $name = substr($routePart, 1);
                $params[$name] = $pathParts[$key];
            }
        }

        return $params;
    }

}