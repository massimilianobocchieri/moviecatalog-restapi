<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use MovieCatalogRestApi\Infrastructure\Interfaces\Http\RouterInterface;
use MovieCatalogRestApi\Resources\Movies\Controllers\FourOuFourController;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\{
    ResponseInterface, ServerRequestInterface
};
use Slim\Http\Response;
use Throwable;

/**
 * Class Router
 *
 * It received the reuqest, find the matching route, applies filters if any and execute the controller,
 * returning the response
 *
 */
class Router implements RouterInterface
{
    private $routes;

    private $container;
    private $config;
    private $logger;


    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->config = $container->get('config');
        $this->logger = $container->get('logger');
    }

    public function route(ServerRequestInterface $request): ResponseInterface
    {
        $response = $this->container->get('response');

        $method = $this->normalizeMethod($request->getMethod());
        $path = $this->cleanFinalSlashIfPresent($request->getUri()->getPath());

        $this->logger->info(sprintf('Received Request to route : %s %s', $method, $path));

        try {
            $this->buildRoutes();
        } catch (Throwable $e) {
            $this->logger->error(sprintf('Error while building routes: %s', $e->getMessage()));
            return (new Response())->withJson(
                ["error" => "Ops we had some trouble while serving your request"],
                500);
        };

        $route = $this->findRoute($method, $path);


        if (null === $route) {
            $this->logger->warn(sprintf('Route not found for request: %s %s', $method, $path));
            return (new FourOuFourController($this->container))($request, $response, []);
        }

        $this->logger->info(sprintf('Found  route : %s %s', implode(", ", $route->getMethods()), $route->getPath()));

        if ($route->hasFilters())
            $request = $this->applyFilters($request, $response, $route->getExtractedParams(), $route->getFilters());

        if ($request->getAttribute('responseError') != null) {
            $this->logger->warn(sprintf('Filter error  in route %s %s for request %s %s', implode(",", $route->getMethods()), $route->getPath(), $method, $path));
            return $request->getAttribute('responseError');
        }

        $response = $this->executeController($route, $request, $response);
        return $response;
    }

    private function normalizeMethod(?string $method = ""): string
    {
        return strtoupper((string)$method);
    }

    private function cleanFinalSlashIfPresent(string $path = "")
    {
        return rtrim($path, '/');
    }

    private function buildRoutes(): void
    {
        if ($this->routes != null) {
            $this->logger->debug('Routes cached...');
            return;
        }

        $this->logger->debug('Building routes...');
        $routesBuilder = new RoutesBuilder($this->config->getAppBasePath() . $this->config->get('routes_file'));
        $this->routes = $routesBuilder->buildRoutes();
        $this->logger->debug(sprintf("%d Routes built correctly", count($this->routes)));
    }

    private function findRoute(string $method, string $path): ?Route
    {
        return (new RouteResolver($this->routes))->resolve($method, $path);
    }

    private function applyFilters(ServerRequestInterface $request, $response, array $extractedParameters = [], array $filters = []): ServerRequestInterface
    {
        foreach ($filters as $filter) {
            $filterName = $this->config->get('filters_namespace') . $filter;
            $filter = new $filterName($this->container);

            $request = $filter($request, $response, $extractedParameters);

            if ($request->getAttribute('responseError') != null)
                return $request;
        }
        return $request;
    }

    private function executeController(Route $route,
                                       ServerRequestInterface $request,
                                       ResponseInterface $response): ResponseInterface
    {
        $controllerName = $this->config->get('controllers_namespace') . $route->getController();
        $controller = new $controllerName($this->container);
        $params = $route->getExtractedParams();

        $response = $controller($request, $response, $params);

        return $response;
    }

}

