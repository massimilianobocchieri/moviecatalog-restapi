<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

/**
 * Class Route
 *
 * Models route info including filter, parameters, associated controller
 * It is also responsible to match against the server request (method, path)
 *
 */
class Route
{
    private $id;

    private $path;
    private $regExprPath;

    private $methods;
    private $controller;
    private $filters;
    private $params;
    private $extractedParams;

    public function __construct(string $id, string $path, array $methods, string $controller)
    {
        $this->id = $id;
        $this->path = $this->regExprPath = $path;

        $this->methods = $methods;
        $this->controller = $controller;

        $this->filters = [];
        $this->params = [];
        $this->extractedParams = [];
    }

    public function getExtractedParams(): array
    {
        return $this->extractedParams;
    }

    public function setExtractedParams(array $extractedParams): void
    {
        $this->extractedParams = $extractedParams;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function getFilters(): array
    {
        return $this->filters;
    }

    public function setFilters(array $filters): void
    {
        $this->filters = $filters;
    }

    public function hasFilters(): bool
    {
        return count($this->filters) > 0;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params = []): void
    {
        $this->params = $params;
    }

    public function matches(string $method, string $path): bool
    {
        return $this->containsMethod($method) && $this->containsPath($path);
    }

    private function containsMethod(string $method): bool
    {
        return in_array($method, $this->getMethods());
    }

    public function getMethods(): array
    {
        return $this->methods;
    }

    private function containsPath(string $path): bool
    {
        return (preg_match("@^" . $this->getRegExprPath() . "$@", $path) > 0);
    }

    public function getRegExprPath(): string
    {
        return $this->regExprPath;
    }

    public function setRegExprPath(string $path): void
    {
        $this->regExprPath = $path;
    }


}