<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use InvalidArgumentException;
use MovieCatalogRestApi\Infrastructure\Exceptions\NotFoundException;
use Throwable;

/**
 * Class RoutesBuilder
 *
 * It is responsible of building the routes with the proper info
 */
class RoutesBuilder
{

    private $jsonRoutes = [];


    public function __construct(string $routesJsonFilePath)
    {
        $this->jsonRoutes = $this->loadRoutesFromJsonFile($routesJsonFilePath);
    }

    private function loadRoutesFromJsonFile(string $routesJsonFilePath)
    {
        try {
            $routesJsonContent = file_get_contents($routesJsonFilePath);
        } catch (Throwable $throw) {
            throw new NotFoundException("file {$routesJsonFilePath} not found", -1, $throw);
        }

        $routesArrayContent = json_decode($routesJsonContent, true);

        if ($this->isRoutesJsonFileContentInvalid($routesJsonContent, $routesArrayContent))
            throw new InvalidArgumentException("Invalid JSON routes file {$routesJsonFilePath} format: {$routesJsonContent}");

        return $routesArrayContent;
    }

    public function buildRoutes(): array
    {
        $routes = [];

        foreach ($this->jsonRoutes as $routeId => $info) {
            $parsedRoute = new Route((string)$routeId, $info['path'], $info['methods'], $info['controller']);

            if (isset($info['filters']))
                $parsedRoute->setFilters($info['filters']);

            if (isset($info['params'])) {
                $parsedRoute->setParams($this->buildParameters($info['params']));
                $parsedRoute->setRegExprPath($this->getRegexPath($parsedRoute->getPath(), $parsedRoute->getParams()));
            }

            $routes[] = $parsedRoute;
        }

        return $routes;
    }

    private function buildParameters(array $parameters = []): array
    {
        $routeParameters = [];

        foreach ($parameters as $parameter)
            $routeParameters[] = new RouteParameter(key($parameter), array_shift($parameter));

        return $routeParameters;
    }

    private function getRegexPath(string $path, array $params): string
    {
        $parsedPath = $path;
        foreach ($params as $param)
            $parsedPath = str_replace(':' . $param->getName(), $param->getRegExpr(), $parsedPath);

        return $parsedPath;
    }

    private function isRoutesJsonFileContentInvalid($routesJsonContent, $routesArrayContent): bool
    {
        return $routesJsonContent != null && $routesArrayContent == null;
    }



}
