<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

/**
 * Class SimpleCollection
 *
 *
 * Type check free Collection to be used the old way
 */
class SimpleCollection extends AbstractTypeCheckedCollection
{
    public function specifyType(): ?string
    {
        return null;
    }
}