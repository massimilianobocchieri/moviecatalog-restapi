<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

use MovieCatalogRestApi\Infrastructure\Exceptions\ContainerException;
use Psr\Container\ContainerInterface;

/**
 * Class DependencyInjectionContainer
 *
 * Sort of Unified services Registry, Dependency Inversion/Injection container, implementing PSR11 FIG Standard
 *
 */
class DependencyInjectionContainer implements ContainerInterface
{
    private $values = [];
    private $keys = [];

    private $defaultSettings = [
        'httpVersion' => '1.1',
        'responseChunkSize' => 4096,
        'addContentLengthHeader' => true
    ];

    public function __construct()
    {
        $this->registerSettings();
        $this->registerDefaultServices();
    }

    private function registerSettings(?array $settings = []): void
    {
        $defaultSettings = $this->defaultSettings;

        $this->set('settings', function () use ($defaultSettings, $settings) {
            return new SimpleCollection(array_merge($settings, $defaultSettings));
        });
    }

    public function set(string $id, callable $value): void
    {
        $this->values[$id] = $value;
        $this->keys[$id] = $value;
    }

    public function get($id)
    {
        if (!$this->has($id))
            throw new ContainerException($id . ' dependency not found');

        return $this->values[$id]($this);
    }

    public function has($id): bool
    {
        return isset($this->keys[$id]);
    }

    private function registerDefaultServices(): void
    {
        (new DefaultServicesProvider())->register($this);
    }

}