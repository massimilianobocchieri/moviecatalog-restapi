<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

use ArrayIterator;
use InvalidArgumentException;
use MovieCatalogRestApi\Infrastructure\Interfaces\CollectionInterface;
use MovieCatalogRestApi\Infrastructure\Interfaces\TypeCheckableCollectionInterface;
use Traversable;

/**
 * Class AbstractTypeCheckedCollection
 *
 * Base class used to implement a sort of TypeChecking ( ~= Generics in Java) on collection items
 *
 */
abstract class AbstractTypeCheckedCollection implements CollectionInterface, TypeCheckableCollectionInterface
{

    protected $data = [];
    private $type;

    public function __construct(array $items = [])
    {
        $this->type = $this->specifyType();
        $this->replace($items);
    }

    public function replace(array $items): void
    {
        foreach ($items as $key => $value)
            $this->set($key, $value);
    }

    public function set($key, $value): void
    {
        $this->checkType($value);
        $this->data[$key] = $value;
    }

    public function checkType($item): bool
    {
        $type = $this->getType();
        if (null === $type)
            return true;

        if (!($item instanceof $type))
            throw new InvalidArgumentException("Parameter must be an {$type} instance");

        return true;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function toArray(): array
    {
        return $this->data;
    }

    public function add($value): void
    {
        $this->set($this->count() + 1, $value);
    }

    public function count(): int
    {
        return count($this->data);
    }

    public function all()
    {
        return $this->data;
    }

    public function delete($item): void
    {
        foreach ($this->data as $key => $currentItem)
            if ($currentItem == $item)
                unset($this->data[$key]);
    }

    public function clear(): void
    {
        $this->data = [];
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->data);
    }

    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }

    public function has($key): bool
    {
        return array_key_exists($key, $this->data);
    }

    public function offsetGet($offset)
    {
        return $this->get($offset);
    }

    public function get($key, $default = null)
    {
        return $this->has($key) ? $this->data[$key] : $default;
    }

    public function offsetSet($offset, $value): void
    {
        $this->set($offset, $value);
    }

    public function offsetUnset($offset): void
    {
        $this->remove($offset);
    }

    public function remove($key): void
    {
        unset($this->data[$key]);
    }

    public function isType(string $type): bool
    {
        return $this->type === $type;
    }

}
