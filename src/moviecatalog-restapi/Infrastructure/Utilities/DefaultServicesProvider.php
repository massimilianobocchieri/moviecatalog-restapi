<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;


use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\IntrospectionProcessor;
use MovieCatalogRestApi\Infrastructure\Http\Router;
use Psr\Container\ContainerInterface;
use Slim\Http\{
    Environment, Headers, Request, Response
};

/**
 * Class DefaultServicesProvider
 * @package MovieCatalogRestApi\Infrastructure\Utilities
 *
 * Used to register minimum services in the container when the application boots
 *
 */
class DefaultServicesProvider
{

    public function register(ContainerInterface $container): void
    {

        $this->addServiceIfNotYetInContainer('config', $container, function () {
            return new Config();
        });

        $this->addServiceIfNotYetInContainer('environment', $container, function () {
            return new Environment($_SERVER);
        });

        $this->addServiceIfNotYetInContainer('request', $container, function () {
            return Request::createFromGlobals($_SERVER);
        });

        $this->addServiceIfNotYetInContainer('response', $container, function ($container) {
            return (new Response(200, new Headers(['Content-Type' => 'text/html; charset=UTF-8'])))
                ->withProtocolVersion($container->get('settings')->get('httpVersion'));
        });

        $this->addServiceIfNotYetInContainer('router', $container, function ($container) {
            return new Router($container);
        });

        $this->addServiceIfNotYetInContainer('logger', $container, function ($container) {
            $config = $container->get('config');
            $logLevel = $config->get('log_level') ?? 'INFO';
            $logger = new Logger('MoviecatalogRestApi');
            $handler = new StreamHandler($config->getAppBasePath() . $config->get('log_file'), $logLevel);
            $formatter = new LineFormatter();
            $formatter->includeStacktraces(true);
            $handler->setFormatter($formatter);
            $logger->pushHandler($handler);
            $logger->pushProcessor(new IntrospectionProcessor($logLevel));
            return $logger;
        });
    }

    private function addServiceIfNotYetInContainer(string $serviceId, ContainerInterface $container, callable $service): void
    {
        if (!$container->has($serviceId))
            $container->set($serviceId, $service);
    }

}