<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

use InvalidArgumentException;
use MovieCatalogRestApi\Infrastructure\Exceptions\NotFoundException;
use Throwable;

/**
 * Class Config
 *
 * Application Configuration utility used to get application configuration properties
 *
 */
class Config
{
    private $data;
    private $appBasePath;

    public function __construct(?string $appBasePath = null)
    {
        $this->appBasePath = $appBasePath ?? __DIR__ . '/../../../../';

        $fileName = $this->appBasePath . 'config/app.json';
        try {
            $configJsonContent = file_get_contents($fileName);
        } catch (Throwable $throw) {
            throw new NotFoundException("file {$fileName} not found", -1, $throw);
        }

        $configArrayContent = json_decode($configJsonContent, true);

        if ($this->isConfigJsonFileContentInvalid($configJsonContent, $configArrayContent))
            throw new InvalidArgumentException("Invalid JSON routes file {$fileName} format: {$configJsonContent}");

        $this->data = $configArrayContent;
    }

    public function get($key)
    {
        if (!isset($this->data[$key])) {
            throw new NotFoundException("Key $key not in config.");
        }
        return $this->data[$key];
    }

    public function getAppBasePath(): string
    {
        return $this->appBasePath;
    }

    private function isConfigJsonFileContentInvalid($configJsonContent, $configArrayContent)
    {
        return $configJsonContent != null && $configArrayContent == null;
    }

}