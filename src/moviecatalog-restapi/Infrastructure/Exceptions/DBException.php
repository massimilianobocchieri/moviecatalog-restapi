<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Exceptions;


use Exception;

class DBException extends Exception
{

}