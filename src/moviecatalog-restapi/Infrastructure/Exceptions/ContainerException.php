<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Exceptions;

use InvalidArgumentException;
use Psr\Container\ContainerExceptionInterface;

class ContainerException extends InvalidArgumentException implements ContainerExceptionInterface
{

}
