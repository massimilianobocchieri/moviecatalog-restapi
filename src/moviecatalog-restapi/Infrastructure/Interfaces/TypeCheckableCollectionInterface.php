<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Interfaces;

interface TypeCheckableCollectionInterface
{
    public function specifyType(): ?string;

    public function checkType($item): bool;

    public function getType(): ?string;

    public function isType(string $type): bool;
}