<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Interfaces\Http;

use Psr\Http\Message\{
    ResponseInterface, ServerRequestInterface
};

/**
 * Interface RouterInterface
 *
 * Routing Interface used to route Connection routing
 *
 */
interface RouterInterface
{
    public function route(ServerRequestInterface $request): ResponseInterface;
}