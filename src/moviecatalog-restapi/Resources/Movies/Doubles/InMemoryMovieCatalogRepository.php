<?php

namespace MovieCatalogRestApi\Resources\Movies\Doubles;

use MovieCatalogRestApi\Resources\Movies\Interfaces\Repositories\MovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieCollection, Person
};

/**
 * Class InMemoryMovieCatalogRepository
 *
 * Double (Mock) movie repository implementation to defect persistence/db during architecture implementation
 *
 */
class InMemoryMovieCatalogRepository implements MovieCatalogRepository
{
    private $movies;

    public function __construct(?MovieCollection $movies = null)
    {
        $this->movies = $movies ?? new MovieCollection();
    }

    public function save(Movie $movie): Movie
    {
        $this->movies->add($movie);
        return $movie;
    }

    public function saveMany(MovieCollection $movies): MovieCollection
    {
        foreach ($movies as $movie)
            $this->movies->add($movie);

        return $movies;
    }

    public function merge(Movie $movie, string $id): ?Movie
    {
        foreach ($this->movies as $currentId => $currentMovie)
            if ($currentMovie->getId() === $id) {
                $this->movies->set($id, $movie);
                return $movie;
            }

        return null;
    }

    public function delete(Movie $movie): void
    {
        $this->movies->delete($movie);
    }

    public function findAllMovies(): MovieCollection
    {
        return $this->movies;
    }

    public function findMoviesByImdbIds(array $imdbIds): MovieCollection
    {
        $movieCollection = new MovieCollection();

        foreach ($this->movies as $currentId => $currentMovie)
            $movieCollection->add($currentMovie);

        return $movieCollection;
    }


    public function findMovieByImdbId(string $imdbId): ?Movie
    {
        foreach ($this->movies as $movie)
            if ($movie->getId() === $imdbId)
                return $movie;

        return null;
    }

    public function findAllMoviesTitle(): array
    {
        $result = [];
        foreach ($this->movies as $movie)
            $result[] = $movie->getTitle();

        return $result;
    }

    public function findMoviesTitleByGenre(Genre $genre): array
    {
        $result = [];

        foreach ($this->movies as $movie)
            if ($movie->getGenre() == $genre->getValue())
                $result[] = $movie->getTitle();

        return $result;
    }

    public function findMoviesTitleByDirector(Person $director): array
    {
        $result = [];

        foreach ($this->movies as $movie)
            foreach ($movie->getDirectors() as $person)
                if ($person == $director)
                    $result[] = $movie->getTitle();

        return $result;
    }

    public function findMoviesTitleByStar(Person $star): array
    {
        $result = [];

        foreach ($this->movies as $movie)
            foreach ($movie->getStars() as $person)
                if ($person == $star)
                    $result[] = $movie->getTitle();

        return $result;
    }

}