<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Mappers;

use InvalidArgumentException;
use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieBuilder, MovieCollection, Person, PersonCollection
};
use Throwable;

/**
 * Trait MovieJsonMapper
 *
 * Utility mapper from/to json for movie/s shared by filters/controllers
 *
 */
trait MovieJsonMapper
{

    public function updateMovieFromJson(?array $json = [], Movie $movie): Movie
    {
        if (isset($json['title']))
            $movie->setTitle($json['title']);
        if (isset($json['genre']))
            $movie->setGenre(new Genre($json['genre']));
        if (isset($json['year']))
            $movie->setYear((int)$json['year']);
        if (isset($json['rating']))
            $movie->setRating((int)$json['rating']);
        if (isset($json['directors']))
            $movie->setDirectors($this->personsFromArray($json['directors']));
        if (isset($json['stars']))
            $movie->setStars($this->personsFromArray($json['stars']));

        return $movie;
    }

    private function moviesToJson(MovieCollection $movies): array
    {
        $data = [];
        foreach ($movies as $movie)
            $data[] = $this->prepareMovie($movie);

        return $data;
    }

    /**
     * @param Movie $movie
     * @return array
     * @throws InvalidArgumentException
     */
    private function prepareMovie(Movie $movie): array
    {
        try {
            return [
                'imdbid' => $movie->getId(),
                "title" => $movie->getTitle(),
                'genre' => $movie->getGenre()->getValue(),
                'year' => $movie->getYear(),
                'rating' => $movie->getRating(),
                'directors' => $this->personsToArray($movie->getDirectors()),
                'stars' => $this->personsToArray($movie->getStars())
            ];
        } catch (Throwable $throw) {
            throw new InvalidArgumentException("Movie is not valid", -1, $throw);
        }
    }

    private function personsToArray(PersonCollection $persons): array
    {
        $result = [];

        foreach ($persons as $person)
            $result[] = ["lastName" => $person->getLastName(), "firstName" => $person->getFirstName()];

        return $result;
    }

    private function moviesFromJson(array $json): MovieCollection
    {
        $movies = new MovieCollection();

        foreach ($json as $jsonMovie)
            $movies->add($this->movieFromJson($jsonMovie));

        return $movies;
    }

    /**
     * @param array $json
     * @return Movie
     * @throws InvalidArgumentException
     */
    private function movieFromJson(array $json): Movie
    {
        try {
            return (new MovieBuilder($json['imdbid']))
                ->title($json['title'])
                ->genre(new Genre($json['genre']))
                ->year((int)$json['year'])
                ->rating((int)$json['rating'])
                ->directors($this->personsFromArray($json['directors']))
                ->stars($this->personsFromArray($json['stars']))
                ->build();
        } catch (Throwable $throw) {
            throw new InvalidArgumentException("Movie Array format is not valid", 0, $throw);
        }
    }

    private function personsFromArray(array $personsAsArray): PersonCollection
    {
        $persons = new PersonCollection();

        foreach ($personsAsArray as $person)
            $persons->add(new Person($person['lastName'], $person['firstName']));

        return $persons;
    }

    private function movieToJson(Movie $movie): array
    {
        return $this->prepareMovie($movie);
    }

    private
    function getMoviesIds(array $moviesArray): array
    {
        $result = [];

        foreach ($moviesArray as $movie)
            $result[] = $movie['imdbid'];

        return $result;
    }

}