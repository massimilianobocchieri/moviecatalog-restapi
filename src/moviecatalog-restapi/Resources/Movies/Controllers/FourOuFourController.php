<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class FourOuFourController extends AbstractController
{
    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        return $this->render($response, ['message' => 'NotFound'], 404);
    }

}
