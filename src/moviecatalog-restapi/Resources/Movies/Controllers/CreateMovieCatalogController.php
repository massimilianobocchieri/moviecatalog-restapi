<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;


use MovieCatalogRestApi\Infrastructure\Exceptions\DBException;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class CreateMovieCatalogController extends AbstractController
{
    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        $statusCode = 201;

        if ($request->getAttribute("movie") == null && $request->getAttribute("movies") == null)
            throw new \InvalidArgumentException("no movie specified to be inserted");

        $data = [];
        if ($request->getAttribute("movie") != null) {
            try {
                $movie = $this->movieCatalogService->createMovie($request->getAttribute("movie"));
                $data = $this->movieToJson($movie);
            } catch (DBException $e) {
                $this->logger->error('error', ['exception' => $e]);
                $statusCode = 500;
                $data = ["message" => "an error occurred while inserting the movie, operation aborted"];
            }
        } else if ($request->getAttribute("movies") != null) {
            try {
                $movies = $this->movieCatalogService->createMovies($request->getAttribute("movies"));
                $data = $this->moviesToJson($movies);
            } catch (DBException $e) {
                $this->logger->error('error', ['exception' => $e]);
                $statusCode = 500;
                $data = ["message" => "an error occurred while inserting the movies, operation aborted"];
            }

        }

        return $this->render($response, $data, $statusCode);
    }

}