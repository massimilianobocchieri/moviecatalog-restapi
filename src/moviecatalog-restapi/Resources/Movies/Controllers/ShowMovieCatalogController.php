<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class ShowMovieCatalogController extends AbstractController
{
    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        if (!isset($args['imdbid']))
            throw new \InvalidArgumentException("imdbid is mandatory");

        $movie = $this->movieCatalogService->getMovieByImdbId($args['imdbid']);

        if ($movie == null)
            list($data, $statusCode) = [["message" => "movie {$args['imdbid']} not found"], 404];
        else
            list($data, $statusCode) = [$this->movieToJson($movie), 200];

        return $this->render($response, $data, $statusCode);
    }
}