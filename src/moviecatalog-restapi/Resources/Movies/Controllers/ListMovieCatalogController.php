<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;


use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class ListMovieCatalogController extends AbstractController
{
    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        $movies = $this->movieCatalogService->getMovies();
        return $this->render($response, $this->moviesToJson($movies));
    }

}