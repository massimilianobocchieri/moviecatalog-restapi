<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Infrastructure\Exceptions\DBException;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class DeleteMovieCatalogController extends AbstractController
{
    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        if (!isset($args['imdbid']))
            throw new \InvalidArgumentException("imdbid is mandatory");

        try {
            $movie = $this->movieCatalogService->deleteMovie($args['imdbid']);
            if ($movie == null)
                list($data, $statusCode) = [["message" => "movie {$args['imdbid']} not found"], 404];
            else
                list($data, $statusCode) = [null, 204];

        } catch (DBException $e) {
            $this->logger->error('error', ['exception' => $e]);
            $statusCode = 500;
            $data = ["message" => "an error occurred while deleting the moviess, operation aborted"];
        }

        return $this->render($response, $data, $statusCode);
    }

}