<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;


use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class UpdateMovieCatalogController extends AbstractController
{
    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        if (!isset($args['imdbid']) || $request->getAttribute("movie") == null)
            throw new \InvalidArgumentException("imdbid and is mandatory and movie must be present");

        try {
            $movie = $this->movieCatalogService->updateMovie($request->getAttribute("movie"), $args["imdbid"]);
            $statusCode = 200;
            $data = $this->movieToJson($movie);
        } catch (DBException $e) {
            $this->logger->error('error', ['exception' => $e]);
            $statusCode = 500;
            $data = ["message" => "an error occurred while deleting the moviess, operation aborted"];
        }

        return $this->render($response, $data, $statusCode);
    }
}