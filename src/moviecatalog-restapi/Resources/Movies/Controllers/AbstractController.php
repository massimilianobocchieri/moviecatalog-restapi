<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

abstract class AbstractController
{
    protected $container;
    protected $movieCatalogService;
    protected $logger;

    public function __construct(DependencyInjectionContainer $container)
    {
        $this->container = $container;
        $this->movieCatalogService = $this->container->get('movieCatalogService');
        $this->logger = $this->container->get('logger');
    }

    public function render(ResponseInterface $response, ?array $data = null, ?int $statusCode = 200): ResponseInterface
    {
        return $response->withJson($data, $statusCode);
    }

    abstract public function __invoke(Request $request, Response $response, array $args): ResponseInterface;

}