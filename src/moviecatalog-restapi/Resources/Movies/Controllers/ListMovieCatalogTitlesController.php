<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;


use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\{
    Request, Response
};

class ListMovieCatalogTitlesController extends AbstractController
{

    use MovieJsonMapper;

    public function __invoke(Request $request, Response $response, array $args): ResponseInterface
    {
        $filters = $request->getAttribute("filter");
        $moviesTitles = $this->movieCatalogService->getMoviesTitle($filters);

        return $this->render($response, $moviesTitles);
    }

}