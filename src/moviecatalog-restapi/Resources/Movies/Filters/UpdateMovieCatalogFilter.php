<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Filters;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use MovieCatalogRestApi\Resources\Movies\Validators\UpdateMovieFilterValidator;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\{
    Request, Response
};

/**
 * Class UpdateMovieCatalogFilter
 *
 * Http filter used to validate update movie action and check whether the entry already exists
 *
 */
class UpdateMovieCatalogFilter
{
    use MovieJsonMapper;

    protected $container;
    protected $movieCatalogService;

    public function __construct(DependencyInjectionContainer $container)
    {
        $this->container = $container;
        $this->movieCatalogService = $this->container->get('movieCatalogService');
    }

    public function __invoke(Request $request, Response $response, array $args): ServerRequestInterface
    {
        $imdbid = $args['imdbid'];

        $bodyPayload = $request->getParsedBody();

        $validator = new UpdateMovieFilterValidator($bodyPayload, $imdbid);
        $validator->validate();

        if (!$validator->isValid())
            return $request->withAttribute('responseError',
                $response->withJson(["validation error" => $validator->getErrorMessage()], 416));

        $movie = $this->movieCatalogService->getMovieByImdbId($imdbid);

        if ($movie == null)
            $request = $request->withAttribute('responseError',
                $response->withJson(["error" => "movie with imdbid {$imdbid} not found, update aborted"], 404));
        else
            $request = $request->withAttribute("movie", $this->updateMovieFromJson($bodyPayload, $movie));

        return $request;
    }
}