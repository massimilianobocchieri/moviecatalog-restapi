<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Filters;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Validators\ListMoviesTitlesFilterValidator;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\{
    Request, Response
};

/**
 * Class ListMovieCatalogTitlesFilter
 *
 * Http Request filter used to validate filter info to search movie titles
 *
 */
class ListMovieCatalogTitlesFilter
{
    protected $container;

    public function __construct(DependencyInjectionContainer $container)
    {
        $this->container = $container;
    }

    public function __invoke(Request $request, Response $response, array $args): ServerRequestInterface
    {
        $bodyPayload = $request->getParsedBody();

        $validator = new ListMoviesTitlesFilterValidator($bodyPayload);
        $validator->validate();

        if (!$validator->isValid())
            $request = $request->withAttribute('responseError',
                $response->withJson(["validation error" => $validator->getErrorMessage()], 416));
        else
            $request = $request->withAttribute("filter", $bodyPayload);

        return $request;
    }
}