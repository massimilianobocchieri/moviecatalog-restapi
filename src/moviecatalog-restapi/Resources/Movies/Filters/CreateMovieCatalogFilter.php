<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Filters;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use MovieCatalogRestApi\Resources\Movies\Model\MovieCollection;
use MovieCatalogRestApi\Resources\Movies\Validators\CreateMovieFilterValidator;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Http\{
    Request, Response
};

/**
 * Class CreateMovieCatalogFilter
 *
 * Http equest Filter used to validate input data and check wether a movie with the same imdbid already exists
 *
 */
class CreateMovieCatalogFilter
{
    use MovieJsonMapper;

    protected $container;
    protected $movieCatalogService;

    public function __construct(DependencyInjectionContainer $container)
    {
        $this->container = $container;
        $this->movieCatalogService = $this->container->get('movieCatalogService');
    }

    public function __invoke(Request $request, Response $response, array $args): ServerRequestInterface
    {
        $bodyPayload = $request->getParsedBody();

        $validator = new CreateMovieFilterValidator($bodyPayload);
        $validator->validate();

        if (!$validator->isValid())
            return $request->withAttribute('responseError',
                $response->withJson(["validation error" => $validator->getErrorMessage()], 416));

        //Single Movie, check if already present in repository
        if ($validator->hasOneMovie())
            return ($this->movieCatalogService->getMovieByImdbId($bodyPayload['imdbid']) != null) ?
                $request->withAttribute('responseError',
                    $response->withJson(["error" => "the movie with imdbid {$bodyPayload['imdbid']} already exists, insertion aborted"], 416))
                :
                $request->withAttribute("movie", $this->movieFromJson($bodyPayload));

        if ($validator->hasMultipleMovies()) {

            $moviesIds = $this->getMoviesIds($bodyPayload);
            $foundMovies = $this->movieCatalogService->getMoviesByIds($moviesIds);
            return ($foundMovies != new MovieCollection())
                ? $request->withAttribute('responseError',
                    $response->withJson(["error" => "one or more movies already exist, insertion aborted"], 416))
                : $request->withAttribute("movies", $this->moviesFromJson($bodyPayload));
        }

    }

}