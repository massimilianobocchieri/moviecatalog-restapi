<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Repositories;

use MongoDB\Collection;
use MovieCatalogRestApi\Infrastructure\Exceptions\DBException;
use MovieCatalogRestApi\Resources\Movies\Interfaces\Repositories\MovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieBuilder, MovieCollection, Person
};

/**
 * Class MongoDbMovieCatalogRepository
 *
 * Repository implementation using mongoDb persistence layer
 *
 * TODO Extend Exception rethrowing on all list* methods and report implement proper handling in Controller layer
 */
class MongoDbMovieCatalogRepository implements MovieCatalogRepository
{
    use MovieJsonMapper;

    private $collection;

    public function __construct(Collection $collection)
    {
        $this->collection = $collection;
    }

    public function save(Movie $movie): Movie
    {
        try {
            $this->collection->insertOne($this->toRecord($movie));
        } catch (\Exception $e) {
            throw new DBException("Insert movie {$movie->getId()} exception",-1,$e);
        }

        return $movie;
    }

    public function saveMany(MovieCollection $movies): MovieCollection
    {
        try {
            $this->collection->insertMany($this->toRecords($movies));
        } catch (\Exception $e) {
            throw new DBException("Insert movies exception",-1,$e);
        }

        return $movies;
    }

    public function merge(Movie $movie, string $id): ?Movie
    {
        try {
            $foundMovie = $this->collection->findOneAndUpdate(
                ['imdbid' => $id],
                ['$set' => $this->toRecord($movie)]
            );
        } catch (\Exception $e) {
            throw new DBException("Update movie {$id} exception",-1,$e);
        }

        return $foundMovie != null ? $movie : null;
    }

    public function delete(Movie $movie): void
    {
        try {
            $this->collection->findOneAndDelete(['imdbid' => $movie->getId()]);
        } catch (\Exception $e) {
            throw new DBException("Delete movie {$movie->getId()} exception",-1,$e);
        }

    }

    public function findAllMovies(): MovieCollection
    {
        $records = $this->collection->find();

        return $this->fromRecords($records);
    }

    public function findAllMoviesTitle(): array
    {
        $result = [];

        $records = $this->collection->find();

        foreach ($records as $record) {
            $result[] = $record['title'];
        }

        return $result;
    }

    public function findMovieByImdbId(string $imdbId): ?Movie
    {
        $record = $this->collection->findOne(['imdbid' => $imdbId]);
        if ($record) {
            return $this->fromRecord($record);
        }

        return null;
    }

    public function findMoviesByImdbIds(array $imdbIds): MovieCollection
    {
        $records = $this->collection->find(['imdbid' => ['$in' => $imdbIds]]);

        return $this->fromRecords($records);
    }

    public function findMoviesTitleByGenre(Genre $genre): array
    {
        $result = [];

        $records = $this->collection->find(["genre" => (string)$genre->getValue()]);
        foreach ($records as $record)
            $result[] = $record['title'];

        return $result;
    }

    public function findMoviesTitleByDirector(Person $director): array
    {
        return $this->findMoviesTitleBy("directors", $director);
    }

    public function findMoviesTitleByStar(Person $star): array
    {
        return $this->findMoviesTitleBy("stars", $star);
    }

    private function findMoviesTitleBy(string $personType, Person $filterPerson): array
    {
        $result = [];

        $records = $this->collection->find();
        foreach ($records as $record) {
            $persons = $this->personsFromArray($record[$personType]->getArrayCopy());
            foreach ($persons as $person)
                if ($person == $filterPerson)
                    $result[] = $record['title'];
        }

        return $result;
    }

    private function toRecords(MovieCollection $movies): array
    {
        $result = [];
        foreach ($movies as $movie)
            $result[] = $this->toRecord($movie);

        return $result;
    }

    private function toRecord(Movie $movie): array
    {

        return [
            'imdbid' => $movie->getId(),
            'title' => $movie->getTitle(),
            'genre' => (string)$movie->getGenre()->getValue(),
            'year' => $movie->getYear(),
            'rating' => $movie->getRating(),
            'directors' => $this->personsToArray($movie->getDirectors()),
            'stars' => $this->personsToArray($movie->getStars())
        ];
    }

    private function fromRecords($records): MovieCollection
    {
        $movies = new MovieCollection();

        foreach ($records as $record)
            $movies->add($this->fromRecord($record));

        return $movies;
    }

    private function fromRecord($record): Movie
    {
        $movie = (new MovieBuilder($record['imdbid']))
            ->title($record['title'])
            ->genre(new Genre($record['genre']))
            ->year($record['year'])
            ->rating($record['rating'])
            ->directors($this->personsFromArray($record['directors']->getArrayCopy()))
            ->stars($this->personsFromArray($record['stars']->getArrayCopy()))
            ->build();
        return $movie;
    }

}