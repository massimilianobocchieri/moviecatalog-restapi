<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Model\Genre;

class UpdateMovieFilterValidator extends AbstractValidator
{
    private $movieSample =
        [
            "title (optional)" => "movie title",
            "genre (optional)" => "movie genre",
            "year (optional)" => "YYYY",
            "rating (optional)" => "a number from 1 to 10",
            "directors (optional)" => "one or more persons",
            "stars (optional)" => "one or more persons"
        ];

    private $imdbid;

    public function __construct($input = [], $imdbid)
    {
        parent::__construct($input);
        $this->imdbid = $imdbid;
    }

    function validate(): void
    {
        if (isset($this->input['imdbid']))
            $this->setError("imdbid not allowed in update movie request");
        else if (!preg_match("@^tt[0-9]{7}$@", $this->imdbid))
            $this->setError("imdbid not valid, tt[0-9]{7} expected");
        else if (isset($this->input["title"]) && empty($this->input["title"]))
            $this->setError("no empty title expected");
        else if (isset($this->input["genre"]) && !Genre::isValid($this->input["genre"]))
            $this->setError("Wrong genre type, allowed values {" . implode(",", Genre::keys()) . "}");
        else if (isset($this->input["year"]) && !is_numeric($this->input["year"]))
            $this->setError("Wrong year value, a valid year expected");
        else if (isset($this->input["rating"]) &&
            (!is_numeric($this->input["rating"]) || $this->input["rating"] <= 0 || $this->input["rating"] > 10))
            $this->setError("Wrong rating value, a valid number expected");
        else if (isset($this->input["directors"]) && !$this->areValidPersons($this->input["directors"]))
            $this->setError("wrong directors type");
        else if (isset($this->input["stars"]) && !$this->areValidPersons($this->input["stars"]))
            $this->setError("wrong stars type");
        else
            $this->valid = true;
    }

    public function getErrorMessage(): ?string
    {
        return $this->valid === true ? null : parent::getErrorMessage() . " - format = " . json_encode($this->movieSample);
    }

}