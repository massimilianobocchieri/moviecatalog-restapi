<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Model\Genre;

class CreateMovieFilterValidator extends AbstractValidator
{
    private $movieSample =
        [
            "imdbid" => "a valid imdbid",
            "title" => "movie title",
            "genre" => "movie genre",
            "year" => "YYYY",
            "rating" => "a number from 1 to 10",
            "directors" => "one or more persons",
            "stars" => "one or more persons"
        ];

    public function __construct($input)
    {
        parent::__construct($input);
    }

    public function validate(): void
    {
        if ($this->hasNoMovies())
            $this->setError("Empty Movie Data");
        else if ($this->hasOneMovie())
            $this->validateSingleMovie();
        else if ($this->hasMultipleMovies())
            $this->validateMultipleMovies();
        else
            $this->setError("Invalid movie data");
    }

    public function hasNoMovies(): bool
    {
        return empty($this->input);
    }

    public function hasOneMovie(): bool
    {
        return (is_array($this->input) && isset($this->input['imdbid']));
    }

    private function validateSingleMovie(?array $movieArray = null): void
    {
        $movieArray = $movieArray ?? $this->input;

        if (!preg_match("@^tt[0-9]{7}$@", $movieArray["imdbid"]))
            $this->setError("imdbid not valid, tt[0-9]{7} expected");
        else if (!isset($movieArray["genre"]))
            $this->setError("Missing genre");
        else if (isset($movieArray["genre"]) && !Genre::isValid($movieArray["genre"]))
            $this->setError("Wrong genre type, allowed values }" . implode(",", Genre::keys()) . "}");
        else if (!isset($movieArray["year"]))
            $this->setError("Missing year");
        else if (!isset($movieArray["rating"]))
            $this->setError("Missing rating");
        else if (!isset($movieArray["directors"]))
            $this->setError("Missing directors");
        else if (isset($movieArray["directors"]) && !$this->areValidPersons($movieArray["directors"]))
            $this->setError("wrong directors type");
        else if (!isset($movieArray["stars"]))
            $this->setError("Missing stars");
        else if (isset($movieArray["stars"]) && !$this->areValidPersons($movieArray["stars"]))
            $this->setError("wrong directors type");
        else
            $this->valid = true;
    }

    public function hasMultipleMovies(): bool
    {
        return is_array($this->input) && isset($this->input[0]) && is_array($this->input[0]);
    }

    private function validateMultipleMovies(): void
    {
        foreach ($this->input as $movieArray) {
            $this->validateSingleMovie($movieArray);
            if (!$this->isValid()) {
                $this->setError("Invalid Movie List");
                return;
            }
        }

        $this->hasMultipleMovies = true;
        $this->valid = true;
    }

    public function getErrorMessage(): ?string
    {
        return $this->valid === true ? null : parent::getErrorMessage() . " - required Movie format = " . json_encode($this->movieSample);
    }

}