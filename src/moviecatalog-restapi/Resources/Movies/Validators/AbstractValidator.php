<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Interfaces\Validators\Validator;

abstract class AbstractValidator implements Validator
{
    protected $input;
    protected $errorMessage;
    protected $valid;

    public function __construct($input)
    {
        $this->input = $input;
        $this->valid = false; //I assume false if not call validate
    }

    public function isValid(): bool
    {
        return $this->valid === true;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function setError(?string $errorMessage = null): void
    {
        $this->valid = false;
        $this->errorMessage = $errorMessage;
    }

    protected function areValidPersons($persons = [])
    {
        if (!is_array($persons))
            return false;
        foreach ($persons as $person)
            if (!isset($person['lastName']))
                return false;
        if (!isset($person['firstName']))
            return false;

        return true;
    }
}