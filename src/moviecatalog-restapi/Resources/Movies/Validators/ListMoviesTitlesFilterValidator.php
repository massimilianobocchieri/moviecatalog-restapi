<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Model\Genre;

class ListMoviesTitlesFilterValidator extends AbstractValidator
{

    public function __construct($input)
    {
        parent::__construct($input);
    }

    function validate(): void
    {
        if (!is_array($this->input))
            $this->setError("Empty Search Filter");
        else if (!isset($this->input['filterBy']))
            $this->setError("filter type missing");
        else if (!isset($this->input['filterValue']))
            $this->setError("filter value missing");
        else if (!in_array($this->input['filterBy'], array('genre', 'director', 'star')))
            $this->setError("wrong filter type");
        elseif ($this->input['filterBy'] == 'genre' && !Genre::isValid($this->input['filterValue']))
            $this->setError("wrong genre value");
        else if ((in_array($this->input['filterBy'], array('star', 'director')) && !is_array($this->input['filterValue'])))
            $this->setError("wrong {$this->input['filterBy']} value");
        else if (in_array($this->input['filterBy'], array('star', 'director')) &&
            (!isset($this->input['filterValue']['firstName']) || !isset($this->input['filterValue']['firstName'])))
            $this->setError("wrong {$this->input['filterBy']} value");
        else
            $this->valid = true;
    }

}