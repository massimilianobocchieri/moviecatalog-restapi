<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use MovieCatalogRestApi\Infrastructure\Utilities\AbstractTypeCheckedCollection;

/**
 * Class PersonCollection
 *
 * Type checked Movie Collection
 */
class MovieCollection extends AbstractTypeCheckedCollection
{
    public function __construct(...$movies)
    {
        parent::__construct($movies);
    }

    public function specifyType(): ?string
    {
        return Movie::class;
    }
}