<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use MyCLabs\Enum\Enum;

/**
 * Class Genre
 *
 * Enum Genre class, thanks to MyCLabs\Enum\Enum
 *
 */
class Genre extends Enum
{
    const HORROR = 'HORROR';
    const DRAMA = 'DRAMA';
    const ACTION = 'ACTION';
    const WESTERN = 'WESTERN';
    const THRILLER = 'THRILLER';
    const ADVENTURE = 'ADVENTURE';
    const DOCUMENTARY = 'DOCUMENTARY';
    const ANIMATION = 'ANIMATION';
    const WAR = 'WAR';
    const CARTOON = 'CARTOON';
    const COMMEDY = 'COMMEDY';
    const SCIENCE = 'SCIENCE';
    const FAMILY = 'FAMILY';
}