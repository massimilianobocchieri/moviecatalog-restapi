<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

/**
 * Class Person
 *
 * Simple entity to represent a person (used for movie directors and stars
 */
class Person
{
    private $lastName;
    private $firstName;

    public function __construct($lastName, $firstName)
    {
        $this->lastName = $lastName;
        $this->firstName = $firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }


}