<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use MovieCatalogRestApi\Infrastructure\Utilities\AbstractTypeCheckedCollection;

/**
 * Class PersonCollection
 *
 * Type checked Person Collection
 */
class PersonCollection extends AbstractTypeCheckedCollection
{

    public function __construct(... $persons)
    {
        parent::__construct($persons);
    }

    public function specifyType(): ?string
    {
        return Person::class;
    }
}