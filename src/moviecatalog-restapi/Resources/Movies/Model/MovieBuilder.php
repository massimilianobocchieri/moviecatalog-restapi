<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

/**
 * Class MovieBuilder
 *
 * Builder for Movie class
 */
class MovieBuilder
{
    public $imDbId;
    public $title;
    public $genre;
    public $year;
    public $rating;
    public $directors;
    public $stars;

    public function __construct(string $imDbId)
    {
        $this->imDbId = $imDbId;
    }

    public function title(string $title): MovieBuilder
    {
        $this->title = $title;
        return $this;
    }

    public function genre(Genre $genre): MovieBuilder
    {
        $this->genre = $genre;
        return $this;
    }

    public function year(int $year): MovieBuilder
    {
        $this->year = $year;
        return $this;
    }

    public function rating(int $rating): MovieBuilder
    {
        $this->rating = $rating;
        return $this;
    }

    public function directors(PersonCollection $directors): MovieBuilder
    {
        $this->directors = $directors;
        return $this;
    }

    public function stars(PersonCollection $stars): MovieBuilder
    {
        $this->stars = $stars;
        return $this;
    }

    public function build(): Movie
    {
        return new Movie($this);
    }

}