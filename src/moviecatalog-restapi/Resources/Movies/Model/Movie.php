<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

/**
 * Class Movie
 *
 * Movie Entity Class
 *
 * Builder patterns has been used for semplicity
 * Object mandatory construction check is executed in constructor
 *
 * @throws \InvalidArgumentException
 *
 */
class Movie extends Entity
{
    private $title;
    private $genre;
    private $year;
    private $rating;
    private $directors;
    private $stars;

    public function __construct(MovieBuilder $builder)
    {
        parent::__construct($builder->imDbId);

        $this->title = $builder->title;
        $this->genre = $builder->genre;
        $this->year = $builder->year;
        $this->rating = $builder->rating;
        $this->directors = $builder->directors;
        $this->stars = $builder->stars;

        if (!$this->isValid())
            throw new \InvalidArgumentException("Movie is not complete, all fields must be filled in");
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getGenre(): Genre
    {
        return $this->genre;
    }

    public function setGenre(Genre $genre): void
    {
        $this->genre = $genre;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function setYear(int $year): void
    {
        $this->year = $year;
    }

    public function getRating(): int
    {
        return $this->rating;
    }

    public function setRating(int $rating): void
    {
        $this->rating = $rating;
    }

    public function getDirectors(): PersonCollection
    {
        return $this->directors;
    }

    public function setDirectors(PersonCollection $directors): void
    {
        $this->directors = $directors;
    }

    public function getStars(): PersonCollection
    {
        return $this->stars;
    }

    public function setStars(PersonCollection $stars): void
    {
        $this->stars = $stars;
    }

    private function isValid(): bool
    {
        if (!preg_match("@^tt[0-9]{7}$@", $this->getId()))
            return false;
        if ($this->title == null)
            return false;
        if ($this->genre == null)
            return false;
        if (!is_numeric($this->year))
            return false;
        if (!is_numeric($this->rating))
            return false;
        if ($this->directors == null)
            return false;
        if ($this->stars == null)
            return false;

        return true;
    }

}