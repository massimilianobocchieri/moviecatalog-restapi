<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

/**
 * Class Entity
 *
 * Bsse Entity class with common data/behavior
 *
 */
class Entity
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): void
    {
        $this->id = $id;
    }

    public function isSame(?Entity $entity): bool
    {
        return $this->id != null && $entity != null && $this->id === $entity->id;
    }

    public function __clone()
    {
        try {
            parent::__clone();
        } catch (\Error $error) {
            if ($error->getMessage() !== "Cannot access parent:: when current class scope has no parent")
                throw new \Error("CloneNotSupportedException", $error->getCode(), $error);
        } catch (\Exception $ex) {
            throw new \Exception("CloneNotSupportedException");
        }
    }
}
