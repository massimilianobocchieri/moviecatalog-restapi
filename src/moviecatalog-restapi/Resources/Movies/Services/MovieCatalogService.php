<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Services;

use MovieCatalogRestApi\Resources\Movies\Interfaces\Repositories\MovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieCollection, Person
};
use Psr\Container\ContainerInterface;
use Throwable;
use UnexpectedValueException;

/**
 * Class MovieCatalogService
 *
 * Service class to handle movies
 *
 */
class MovieCatalogService
{
    private $container;
    private $movieRepository;
    private $logger;

    public function __construct(ContainerInterface $container, MovieCatalogRepository $movieRepository)
    {
        $this->container = $container;
        $this->logger = $this->container->get('logger');
        $this->movieRepository = $movieRepository;
    }

    public function getMovies(): MovieCollection
    {
        return $this->movieRepository->findAllMovies();
    }

    public function getMoviesTitle(array $filter = []): array
    {
        if (!$filter)
            return $this->movieRepository->findAllMoviesTitle();

        if ($filter['filterBy'] == 'genre') {
            $genre = null;
            try {
                $genre = new Genre($filter['filterValue']);
            } catch (UnexpectedValueException $e) {
                $this->logger->error($e->getMessage());
                return [];
            }

            return $this->movieRepository->findMoviesTitleByGenre($genre);
        }

        if (in_array($filter['filterBy'], ["director", "star"]))
            try {
                $person = new Person($filter['filterValue']['lastName'], $filter['filterValue']['firstName']);
            } catch (Throwable  $e) {
                $this->logger->error($e->getMessage());
                return [];
            }

        if ($filter['filterBy'] == 'director')
            return $this->movieRepository->findMoviesTitleByDirector($person);

        if ($filter['filterBy'] == 'star')
            return $this->movieRepository->findMoviesTitleByStar($person);

        $this->logger->warning("Invalid filter");
        return [];
    }

    public function getMovieByImdbId(string $imdbId): ?Movie
    {
        return $this->movieRepository->findMovieByImdbId($imdbId);
    }

    public function getMoviesByIds(array $ids): MovieCollection
    {
        return $this->movieRepository->findMoviesByImdbIds($ids);
    }

    public function createMovie(Movie $movie): Movie
    {
        return $this->movieRepository->save($movie);
    }

    public function createMovies(MovieCollection $movies): MovieCollection
    {
        return $this->movieRepository->saveMany($movies);
    }

    public function updateMovie(Movie $movie, string $imdbId): ?Movie
    {
        return $this->movieRepository->merge($movie, $imdbId);
    }

    public function deleteMovie(string $imdbId): ?Movie
    {
        $movie = $this->movieRepository->findMovieByImdbId($imdbId);
        if ($movie != null)
            $this->movieRepository->delete($movie);
        return $movie;
    }

}