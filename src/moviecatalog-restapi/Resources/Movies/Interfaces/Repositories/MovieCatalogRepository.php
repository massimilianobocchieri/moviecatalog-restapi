<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Interfaces\Repositories;

use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieCollection, Person
};

/**
 * Interface MovieCatalogRepository
 *
 * Movie Repository "Contract"
 *
 */
interface MovieCatalogRepository
{
    function save(Movie $movie): Movie;

    function saveMany(MovieCollection $movies): MovieCollection;

    function merge(Movie $movie, string $id): ?Movie;

    function delete(Movie $movie): void;

    function findAllMovies(): MovieCollection;

    function findAllMoviesTitle(): array;

    function findMovieByImdbId(string $imdbId): ?Movie;

    function findMoviesTitleByGenre(Genre $genre): array;

    function findMoviesTitleByDirector(Person $director): array;

    function findMoviesTitleByStar(Person $star): array;
}
