<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Interfaces\Validators;

/**
 * Interface Validator
 *
 * Generic validator "Contract"
 */
interface Validator
{
    function validate(): void;

    function isValid(): bool;

    function setError(?string $message): void;

    function getErrorMessage(): ?string;

}