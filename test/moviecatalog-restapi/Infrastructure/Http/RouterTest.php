<?php
declare(strict_types=1);

namespace Infrastructure\Http;

use MovieCatalogRestApi\Infrastructure\Http\Router;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Doubles\InMemoryMovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

class RouterTest extends TestCase
{
    private $router;
    private $container;

    public function setUp()
    {
        $this->container = new DependencyInjectionContainer();
        $this->container->set("movieCatalogService", function ($container) {
            return new MovieCatalogService($this->container, new InMemoryMovieCatalogRepository());
        });

        $this->router = new Router($this->container);
    }

    public function testCanCreateRouter()
    {
        $this->assertInstanceOf(Router::class, $this->router);
    }

    public function testCanRoute()
    {
        $this->assertInstanceOf(Response::class,
            $this->router->route(Request::createFromGlobals(Environment::mock($_SERVER))));
    }

    public function testFourOFOurResponseForNotMappedUri()
    {
        $response = $this->router->route(Request::createFromGlobals([Environment::mock(["REQUEST_URI" => "/notmapped", "REQUEST_METHOD" => "GET"])]));
        $this->assertEquals(json_encode(["message" => "NotFound"]), (string)$response->getBody());
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testMoviesGe()
    {
        $response = $this->router->route(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "GET", "REQUEST_URI" => "/api/movies"])));
        $this->assertEquals("[]", (string)$response->getBody());
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testMoviePostWithNoDataRaisesForOuSixsteen()
    {
        $response = $this->router->route(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"])));
        $this->assertStringStartsWith("{\"validation error\":\"Empty Movie Data", (string)$response->getBody());
        $this->assertEquals(416, $response->getStatusCode());
    }

    public function testMoviePutWithNoDataAndNotExistingRaisesForOuFour()
    {
        $response = $this->router->route(Request::createFromGlobals(Environment::mock(
            ["REQUEST_METHOD" => "PUT", "REQUEST_URI" => "/api/movies/tt1234567"])));
        $this->assertEquals(404, $response->getStatusCode());
    }

}
