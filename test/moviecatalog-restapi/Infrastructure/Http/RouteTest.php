<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use PHPUnit\Framework\TestCase;

class RouteTest extends TestCase
{
    public function testCanCreate()
    {
        $this->assertInstanceOf(Route::class, new Route("1", "/api/movies/", ["POST"], 'a controller'));
    }

    public function testNoMatchReturnsFalse()
    {
        $sampleRoute = new Route("1", "/api/movies/", ["POST"], 'a controller');

        $this->assertFalse($sampleRoute->matches("PUT", "/api/movies"));
    }

    public function testEmptyRequestReturnsFalse()
    {
        $sampleRoute = new Route("1", "/api/movies/", ["POST"], 'a controller');

        $this->assertFalse($sampleRoute->matches("", ""));
    }

    public function testMatchOnlyPathReturnsNull()
    {
        $sampleRoute = new Route("1", "/api/movies", ["GET"], 'a controller');

        $this->assertFalse($sampleRoute->matches("PUT", "/api/movies"));
    }

    public function testMatchOnlyMethodReturnsNull()
    {
        $sampleRoute = new Route("1", "/api/movies", ["GET"], 'a controller');

        $this->assertFalse($sampleRoute->matches("PUT", "/anotherpath"));
    }

    public function testMatchReturnsTrue()
    {
        $sampleRoute = new Route("1", "/api/movies", ["GET"], 'a controller');

        $this->assertTrue($sampleRoute->matches("GET", "/api/movies"));
    }

    public function testMatchDynamicRouteReturnsTrue()
    {
        $sampleRoute = new Route("1", "/api/movies/:imdbid", ["POST"], 'a controller');
        $sampleRoute->setRegExprPath("/api/movies/tt[0-9]{7}");
        $this->assertTrue($sampleRoute->matches("POST", "/api/movies/tt1234567"));
    }

    public function testDynamicRouteWithDifferentRegexprReturnsFalse()
    {
        $sampleRoute = new Route("1", "/api/movies/:imdbid", ["POST"], 'a controller');
        $sampleRoute->setRegExprPath("/api/movies/[\d]+");
        $this->assertFalse($sampleRoute->matches("POST", "/api/movies/tt1234567"));
    }

}
