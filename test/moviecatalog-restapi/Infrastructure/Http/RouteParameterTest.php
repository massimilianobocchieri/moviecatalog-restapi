<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class RouteParameterTest extends TestCase
{

    public function testCanCreateRouteParameter()
    {
        $routeParameter = new RouteParameter("name", "NUMBER");
        $this->assertNotNull($routeParameter);
        $this->assertInstanceOf(RouteParameter::class, $routeParameter);
    }

    /**
     * @expectedException     InvalidArgumentException
     */
    public function testWrongTypeThrowsInvalidArgumentException()
    {
        new RouteParameter("name", "typenotallowed");
    }
}
