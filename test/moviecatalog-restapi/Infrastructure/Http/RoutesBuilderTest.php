<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use PHPUnit\Framework\TestCase;

class RoutesBuilderTest extends TestCase
{

    private $container;
    private $config;
    private $routesFilePath;
    private $testInvalidroutesJsonFilePath = __DIR__ . "/../../config/routes_file_wrong_format.json";

    public function setUp()
    {
        $this->container = new DependencyInjectionContainer();
        $this->config = $this->container->get('config');
        $this->routesFilePath = $this->config->getAppBasePath() . $this->config->get('routes_file');
    }

    /**
     * @expectedException \ArgumentCountError
     */
    public function testCannotCreateWithoutRoutesFilePath()
    {
        $this->assertInstanceOf(RoutesBuilder::class, new RoutesBuilder());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCannotCreateWitInvalidRoutesFilePath()
    {
        $this->assertInstanceOf(RoutesBuilder::class, new RoutesBuilder($this->testInvalidroutesJsonFilePath));
    }

    public function testCanCreate()
    {
        $this->assertInstanceOf(RoutesBuilder::class, new RoutesBuilder($this->routesFilePath));
    }

    public function testBuildRoutesReturnsRoutes()
    {
        $routesBuilder = new RoutesBuilder($this->routesFilePath);
        $routes = $routesBuilder->buildRoutes();

        $this->assertNotNull($routes);
        $this->assertContainsOnlyInstancesOf(Route::class, $routes);
    }

}
