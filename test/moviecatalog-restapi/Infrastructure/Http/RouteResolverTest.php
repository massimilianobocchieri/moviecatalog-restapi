<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Http;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use PHPUnit\Framework\TestCase;

class RouteResolverTest extends TestCase
{

    private $routes;

    public function setUp()
    {
        $container = new DependencyInjectionContainer();
        $config = $container->get('config');
        $routesFilePath = $config->getAppBasePath() . $config->get('routes_file');
        $routesBuilder = new RoutesBuilder($routesFilePath);
        $this->routes = $routesBuilder->buildRoutes();
    }

    /**
     * @expectedException \ArgumentCountError
     */
    public function testCannotCreateWithoutRoutes()
    {
        $this->assertInstanceOf(RouteResolver::class, new RouteResolver());
    }

    public function testCanCreate()
    {
        $this->assertInstanceOf(RouteResolver::class, new RouteResolver([]));
    }

    public function testWhenEmptyRoutesResolveReturnsNull()
    {
        $routeResolver = new RouteResolver([]);

        $this->assertNull($routeResolver->resolve("GET", "ap"));
    }

    public function testWithNotMatchingPathResolveReturnsNull()
    {
        $routeResolver = new RouteResolver($this->routes);

        $this->assertNull($routeResolver->resolve("GET", "notmatchingpath"));
    }

    public function testWithNotMatchingMethodResolveReturnsNull()
    {
        $routeResolver = new RouteResolver($this->routes);

        $this->assertNull($routeResolver->resolve("DELETE", "/api/movies"));
    }


    public function testWhenMatchingMethodAndPathReturnsARoute()
    {
        $routeResolver = new RouteResolver($this->routes);

        $this->assertInstanceOf(Route::class, $routeResolver->resolve("GET", "/api/movies"));
    }

    public function testWhenMatchingMethodAndPathForDynamicRoutesRouteReturnedContainsMatchedParams()
    {
        $routeResolver = new RouteResolver($this->routes);
        $foundRoute = $routeResolver->resolve("GET", "/api/movies/tt1234567");
        $this->assertEquals(["imdbid" => "tt1234567"], $foundRoute->getExtractedParams());
    }

}
