<?php
declare(strict_types=1);

namespace Infrastructure\Utilities;

use MovieCatalogRestApi\Infrastructure\Http\Router;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Slim\Http\{
    Request, Response
};

class DependencyInjectionContainerTest extends TestCase
{

    private $container;

    public function setUp(): void
    {
        $this->container = new DependencyInjectionContainer();
    }

    public function testCanCreateDependencyInjectionContainer()
    {
        $this->assertNotNull($this->container);
        $this->assertInstanceOf(ContainerInterface::class, $this->container);
        $this->assertInstanceOf(DependencyInjectionContainer::class, $this->container);
    }

    public function testWhenCreatedContainerHasRequestService()
    {
        $this->assertInstanceOf(Request::class, $this->container->get('request'));
    }

    public function testWhenCreatedContainerHasResponseService()
    {
        $this->assertInstanceOf(Response::class, $this->container->get('response'));
    }

    public function testWhenCreatedContainerHasRouterService()
    {
        $this->assertInstanceOf(Router::class, $this->container->get('router'));
    }

    /**
     * @expectedException MovieCatalogRestApi\Infrastructure\Exceptions\ContainerException
     */
    public function testGetWithNonExistingValueThrowsEXception()
    {
        $this->container->get("doesnexist");
    }

    public function testWhenSetAValueThenHasReturnsTrueAndGetReturnsValue()
    {
        $this->container->set('key', function(){});
        $this->assertTrue($this->container->has('key'));
        $this->assertNotNull((string)$this->container->get('key'));
    }

}
