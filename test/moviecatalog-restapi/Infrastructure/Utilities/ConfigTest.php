<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

use InvalidArgumentException;
use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{

    private $config;

    public function setUp()
    {
        $this->config = new Config();
    }

    /**
     * @expectedException MovieCatalogRestApi\Infrastructure\Exceptions\NotFoundException
     */
    public function testWhenGettingANotExistingEntryExceptionIsThrown()
    {
        $this->config->get("doesntexist");
    }

    /**
     * @expectedException MovieCatalogRestApi\Infrastructure\Exceptions\NotFoundException
     */
    public function testWhenInstantiatingConfigWithNonExistingFilePathExceptionIsThrown()
    {
        (new Config("notexistingpath"));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testWhenInstantiatingConfigWithInvalidJsonFormatExceptionIsThrown()
    {
        (new Config(__DIR__ . "/../../"));
    }

    public function testWhenGettingAnExistingEntryValueIsReturned()
    {
        $this->assertNotNull($this->config->get("db"));
    }

    public function testWhenGettingAppBasePathValueIsReturned()
    {
        $this->assertNotNull($this->config->getAppBasePath());
    }

}
