<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Infrastructure\Utilities;

use Monolog\Logger;
use MovieCatalogRestApi\Infrastructure\Http\Router;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

class DefaultServicesProviderTest extends TestCase
{

    public function testCanCreate()
    {
        $this->assertInstanceOf(DefaultServicesProvider::class, new DefaultServicesProvider());
    }

    public function testWhenCalledRegisterItWillInstantiateContainerDefaultService()
    {
        $defaultServiceProvider = new DefaultServicesProvider();
        $container = new DependencyInjectionContainer();

        $defaultServiceProvider->register($container);

        $this->assertTrue($container->has('config'));
        $this->assertInstanceOf(Config::class, ($container->get('config')));

        $this->assertTrue($container->has('environment'));
        $this->assertInstanceOf(Environment::class, ($container->get('environment')));

        $this->assertTrue($container->has('request'));
        $this->assertInstanceOf(Request::class, ($container->get('request')));

        $this->assertTrue($container->has('response'));
        $this->assertInstanceOf(Response::class, ($container->get('response')));

        $this->assertTrue($container->has('router'));
        $this->assertInstanceOf(Router::class, ($container->get('router')));

        $this->assertTrue($container->has('logger'));
        $this->assertInstanceOf(Logger::class, ($container->get('logger')));

    }
}
