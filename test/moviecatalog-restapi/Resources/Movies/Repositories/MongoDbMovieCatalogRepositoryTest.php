<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Repositories;

use Hamcrest\Matchers;
use Mockery;
use MongoDB\Collection;
use MovieCatalogRestApi\Resources\Movies\Model\Movie;
use MovieCatalogRestApi\Resources\Movies\Model\MovieCollection;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;

class MongoDbMovieCatalogRepositoryTest extends TestCase
{

    private $collectionMock;
    private $testSetup;
    private $repository;

    private $sampleMovie;
    private $anotherSsampleMovie;


    public function testCanCreate()
    {
        return $this->assertInstanceOf(MongoDbMovieCatalogRepository::class, $this->repository);
    }


    public function testCanSaveOk()
    {
        $this->assertInstanceOf(Movie::class, $this->repository->save($this->sampleMovie));
    }

    /**
     * @expectedException \MovieCatalogRestApi\Infrastructure\Exceptions\DBException
     */
    public function testWhenSavingWithExceptionADBExceptionIsThrown()
    {
        $this->repository->save($this->anotherSsampleMovie);
    }

    public function testCanSaveManyOk()
    {
        $this->assertInstanceOf(MovieCollection::class, $this->repository->saveMany($this->testSetup->getSampleMovies()));
    }

    /**
     * @expectedException \MovieCatalogRestApi\Infrastructure\Exceptions\DBException
     */
    public function testWhenSavingManyWithExceptionADBExceptionIsThrown()
    {
        $this->repository->saveMany($this->testSetup->getAnotherSampleMovies());
    }

    public function testCanMergeOk()
    {
        $this->assertInstanceOf(Movie::class, $this->repository->merge($this->testSetup->getSampleMovie(), TestSetup::getSampleImDbId()));
    }

    /**
     * @expectedException \MovieCatalogRestApi\Infrastructure\Exceptions\DBException
     */
    public function testWhenMergingWithExceptionADBExceptionIsThrown()
    {
        $this->repository->merge($this->testSetup->getAnotherSampleMovie(), $this->testSetup->getAnotherSampleMovie()->getId());
    }

    public function testCanDeleteOk()
    {
        $this->assertNull($this->repository->delete($this->testSetup->getSampleMovie()));
    }

    /**
     * @expectedException \MovieCatalogRestApi\Infrastructure\Exceptions\DBException
     */
    public function testWhenDeletingWithExceptionADBExceptionIsThrown()
    {
        $this->repository->delete($this->testSetup->getAnotherSampleMovie());
    }

    public function testCanFindAll()
    {
        $this->assertInstanceOf(MovieCollection::class, $this->repository->findAllMovies());
    }

    public function testCanFindAllMoviesTitle()
    {
        $this->assertInternalType('array', $this->repository->findAllMoviesTitle());
    }

    public function testWhenMovieDoesntExistFindByIdReturnsNull()
    {
        $this->assertNull($this->repository->findMovieByImdbId($this->anotherSsampleMovie->getId()));
    }


    public function setUp()
    {
        $this->testSetup = new TestSetup();
        $this->sampleMovie = $this->testSetup->getSampleMovie();
        $this->anotherSsampleMovie = $this->testSetup->getAnotherSampleMovie();


        $this->collectionMock = Mockery::mock(Collection::class);

        $this->collectionMock->allows()->insertOne()
            ->with(Matchers::equalTo(TestSetup::getSampleMovieAsArray()))
            ->andReturns(null);

        $this->collectionMock->allows()->insertOne()
            ->with(Matchers::equalTo(TestSetup::getAnotherSampleMovieAsArray()))
            ->andThrow(\Exception::class);


        $this->collectionMock->allows()->insertMany()
            ->with(Matchers::equalTo(TestSetup::getSampleMoviesAsArray()))
            ->andReturns(null);

        $this->collectionMock->allows()->insertMany()
            ->with(Matchers::equalTo(TestSetup::getAnotherSampleMovieAsArray()))
            ->andThrow(\Exception::class);


        $this->collectionMock->allows()->findOneAndUpdate(
            ['imdbid' => TestSetup::getSampleImDbId()],
            ['$set' => TestSetup::getSampleMovieAsArray()])
            ->andReturns(TestSetup::getSampleMovieAsArray());


        $this->collectionMock->allows()->findOneAndDelete()
            ->with(Matchers::equalTo(['imdbid' => $this->sampleMovie->getId()]))
            ->andReturns(null);

        $this->collectionMock->allows()->findOneAndDelete()
            ->with(Matchers::equalTo(['imdbid' => $this->anotherSsampleMovie->getId()]))
            ->andThrow(\Exception::class);

        $this->collectionMock->expects('find')
            ->andReturns([]);

        $this->collectionMock->allows()->find()
            ->with(Matchers::equalTo(['imdbid' => ['$in' => ["tt0120587", "tt0112573"]]]))
            ->andReturns([]);

        $this->collectionMock->allows()->find(["genre" => $this->sampleMovie->getGenre()->getValue()])
            ->andReturns([]);

        $this->collectionMock->allows()->findOne()
            ->with(Matchers::equalTo(["imdbid" => $this->anotherSsampleMovie->getId()]))
            ->andReturns(null);

        $this->repository = new MongoDbMovieCatalogRepository($this->collectionMock);
    }


}
