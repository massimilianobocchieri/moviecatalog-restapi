<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Interfaces\Validators\Validator;
use PHPUnit\Framework\TestCase;

class ListMoviesTitlesFilterValidatorTest extends TestCase
{

    public function testCanCreate()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator([]);
        $this->assertInstanceOf(Validator::class, $listMovieTitlesFilterValidator);
    }

    public function testWhenCallingIsValidBeforeValidationValidationFails()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "genre", "filterValue" => "ACTION"]
        );
        $this->assertFalse($listMovieTitlesFilterValidator->isValid());
    }

    public function testWithValidFilterByGenreValidationSucceeds()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "genre", "filterValue" => "ACTION"]
        );

        $listMovieTitlesFilterValidator->validate();

        $this->assertTrue($listMovieTitlesFilterValidator->isValid());
    }

    public function testWithValidFilterByDirectorValidationSucceeds()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "director",
                "filterValue" => ["lastName" => "Burton", "firstName" => "Tim"]
            ]
        );

        $listMovieTitlesFilterValidator->validate();

        $this->assertTrue($listMovieTitlesFilterValidator->isValid());
    }

    public function testWithValidFilterByStarValidationSucceeds()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "star",
                "filterValue" => ["lastName" => "Forty", "firstName" => "Phil"]
            ]
        );

        $listMovieTitlesFilterValidator->validate();

        $this->assertTrue($listMovieTitlesFilterValidator->isValid());
    }

    public function testWhenValidatingEmptyFilterValidationFails()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator([]);

        $listMovieTitlesFilterValidator->validate();
        $this->assertFalse($listMovieTitlesFilterValidator->isValid());
    }

    public function testWhenValidatingInvalidGenreValidationFails()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "genre", "filterValue" => "thisgenreisinvalid"]);

        $listMovieTitlesFilterValidator->validate();
        $this->assertFalse($listMovieTitlesFilterValidator->isValid());
    }

    public function testWhenValidatingInvalidDirectorValidationFails()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "director", "filterValue" => "notavalidperson"]);

        $listMovieTitlesFilterValidator->validate();
        $this->assertFalse($listMovieTitlesFilterValidator->isValid());
    }

    public function testWhenValidatingInvalidStarValidationFails()
    {
        $listMovieTitlesFilterValidator = new ListMoviesTitlesFilterValidator(
            ["filterBy" => "director", "filterValue" => "notavalidperson"]);

        $listMovieTitlesFilterValidator->validate();
        $this->assertFalse($listMovieTitlesFilterValidator->isValid());
    }

}
