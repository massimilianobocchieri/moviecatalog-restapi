<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\Resources\Movies\Interfaces\Validators\Validator;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;

class UpdateMovieFilterValidatorTest extends TestCase
{
    private $sampleMovieUpdateInfo;
    private $sampleImdbId;

    public function setUp()
    {
        $this->sampleMovieUpdateInfo = TestSetup::getMovieUpdateInfo();
        $this->sampleImdbId = TestSetup::getSampleImDbId();
    }

    public function testCanCreate()
    {
        self::assertInstanceOf(Validator::class, new UpdateMovieFilterValidator([], $this->sampleImdbId));
    }

    public function testWhenValidatingAnEmptyArrayValidationSucceeds()
    {
        $validator = new UpdateMovieFilterValidator([], $this->sampleImdbId);
        $validator->validate();
        self::assertTrue($validator->isValid());
    }

    public function testWhenValidatingAnEmptyArrayWithInvalidImdbIdValidationFails()
    {
        $validator = new UpdateMovieFilterValidator([], "itsnotavalidimdbid");
        $validator->validate();
        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAnEmptyArrayNoErrorMessageIsReturned()
    {
        $validator = new UpdateMovieFilterValidator([], $this->sampleImdbId);
        $validator->validate();
        self::assertNull($validator->getErrorMessage());
    }

    public function testWhenValidatinAgValidMovieBeforeCallingValidateReturnsFalse()
    {
        $validator = new UpdateMovieFilterValidator($this->sampleMovieUpdateInfo, $this->sampleImdbId);

        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAgInvalidMovieIdBeReturnsFalse()
    {
        $validator = new UpdateMovieFilterValidator($this->sampleMovieUpdateInfo, "invalidimdbid");

        $validator->validate();

        self::assertFalse($validator->isValid());
    }


    public function testWhenValidatingAgInvalidMovieGenreReturnsFalse()
    {
        $invalidMovieInfo = $this->sampleMovieUpdateInfo;
        $invalidMovieInfo['genre'] = "invalid";
        $validator = new UpdateMovieFilterValidator($invalidMovieInfo, $this->sampleImdbId);

        $validator->validate();

        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAgInvalidMovieYearReturnsFalse()
    {
        $invalidMovieInfo = $this->sampleMovieUpdateInfo;
        $invalidMovieInfo['year'] = "invalidyear";
        $validator = new UpdateMovieFilterValidator($invalidMovieInfo, $this->sampleImdbId);

        $validator->validate();

        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAgInvalidMovieRatingReturnsFalse()
    {
        $invalidMovieInfo = $this->sampleMovieUpdateInfo;
        $invalidMovieInfo['rating'] = 11;
        $validator = new UpdateMovieFilterValidator($invalidMovieInfo, $this->sampleImdbId);

        $validator->validate();

        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAgInvalidMovieDirectorsReturnsFalse()
    {
        $invalidMovieInfo = $this->sampleMovieUpdateInfo;
        $invalidMovieInfo['directors'] = ["wrong directors format"];
        $validator = new UpdateMovieFilterValidator($invalidMovieInfo, $this->sampleImdbId);

        $validator->validate();

        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAgInvalidMovieStarsReturnsFalse()
    {
        $invalidMovieInfo = $this->sampleMovieUpdateInfo;
        $invalidMovieInfo['stars'] = ["wrong stars format"];
        $validator = new UpdateMovieFilterValidator($invalidMovieInfo, $this->sampleImdbId);

        $validator->validate();

        self::assertFalse($validator->isValid());
    }

    public function testWhenNotValidErrorMessageIsNotNull()
    {
        $validator = new UpdateMovieFilterValidator(["rating" => "whoknows"], $this->sampleImdbId);
        $validator->validate();
        $this->assertNotNull($validator->getErrorMessage());
    }

    public function testWhenSettingErrorIsValidReturnsFalse()
    {
        $validator = new UpdateMovieFilterValidator(["rating" => "whoknows"], $this->sampleImdbId);
        $validator->validate();
        $validator->setError("wrong data");
        self::assertFalse($validator->isValid());
    }
}
