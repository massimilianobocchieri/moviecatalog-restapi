<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;

class CreateMovieFilterValidatorTest extends TestCase
{
    private $sampleMovieAsArray;
    private $sampleMoviesAsArray;


    public function setUp()
    {
        $this->sampleMovieAsArray = TestSetup::getSampleMovieAsArray();
        $this->sampleMoviesAsArray = TestSetup::getSampleMoviesAsArray();
    }

    public function testCanCreate()
    {
        self::assertInstanceOf(AbstractValidator::class, new CreateMovieFilterValidator([]));
    }

    public function testWhenValidatingAnEmptyArrayValidationFails()
    {
        $validator = new CreateMovieFilterValidator([]);
        $validator->validate();
        self::assertFalse($validator->isValid());
    }

    public function testWhenValidatingAnEmptyArrayErrorMessageIsPresent()
    {
        $validator = new CreateMovieFilterValidator([]);
        $validator->validate();
        self::assertNotNull($validator->getErrorMessage());
    }

    public function testWhenValidatingAValidSingleMovieValidationSucceeds()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMovieAsArray);
        $validator->validate();
        self::assertTrue($validator->isValid());
        self:
        self::assertNull($validator->getErrorMessage());
    }

    public function testWhenValidatingAValidSingleMovieErrorMessageIsNull()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMovieAsArray);
        $validator->validate();
        self::assertNull($validator->getErrorMessage());
    }

    public function testWhenValidatingAValidMovieSaysItHasOneMovie()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMovieAsArray);
        self::assertTrue($validator->hasOneMovie());
    }

    public function testWhenValidatinAgValidMovieBeforeCallingValidateReturnsFalse()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMovieAsArray);
        self::assertFalse($validator->isValid());
        $validator->validate();
    }

    public function testWhenValidatinAValidMovieBeforeCallingValidateHasErrorMessage()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMovieAsArray);
        self::assertFalse($validator->isValid());
        self::assertNotNull($validator->getErrorMessage());
        $validator->validate();
    }


    public function testWhenValidatingValidMultipleMoviesValidationSucceeds()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMoviesAsArray);
        $validator->validate();
        self::assertTrue($validator->isValid());
        self::assertNull($validator->getErrorMessage());
    }

    public function testWhenValidatingValidMultipleMoviesSaysItHasMultipleMovies()
    {
        $validator = new CreateMovieFilterValidator($this->sampleMoviesAsArray);
        self::assertTrue($validator->hasMultipleMovies());
    }

}
