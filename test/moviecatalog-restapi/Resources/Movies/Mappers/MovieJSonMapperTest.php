<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Mappers;

use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, Movie, MovieBuilder, MovieCollection, Person, PersonCollection
};
use PHPUnit\Framework\TestCase;

class MovieJsonMapperTest extends TestCase
{
    use MovieJsonMapper;

    private $sampleMovies;
    private $sampleMovie;


    public function setUp()
    {
        $this->sampleMovie = self::buildSampleMovie();
        $this->sampleMovies = self::buildSampleMovies();
    }

    private static function buildSampleMovie(): Movie
    {
        return (new MovieBuilder("tt0120587"))
            ->title("AntZ")
            ->genre(new Genre(Genre::ADVENTURE))
            ->year(2018)
            ->rating(5)
            ->directors(new PersonCollection(
                new Person("Darnell", "Eric"),
                new Person("Johnson", "Tim")
            ))
            ->stars(new PersonCollection(
                new Person("Darnell", "Eric"),
                new Person("Johnson", "Tim")
            ))
            ->build();
    }

    private static function buildSampleMovies(): MovieCollection
    {
        $sampleMovieCollection = new MovieCollection();
        $sampleMovie = self::buildSampleMovie();
        $movieId = $sampleMovie->getId();
        $idPrefix = substr($movieId, 0, 4);
        $idPostfix = (int)substr($movieId, 3, 3);

        for ($i = 1; $i <= 10; $i++) {
            $movie = clone $sampleMovie;
            $movie->setId($idPrefix . (string)($idPostfix + $i));
            $sampleMovieCollection->add($movie);
        }

        return $sampleMovieCollection;
    }

    public function testMoviesToJsonWithEmptyCollectionReturnsEmptyArray()
    {
        self::assertEquals([], $this->moviesToJson(new MovieCollection()));
    }

    public function testMoviesToJsonWithOneMovieReturnsOneElement()
    {
        self::assertEquals(1, count($this->moviesToJson(new MovieCollection($this->sampleMovie))));
    }

    /**
     * @expectedException     \InvalidArgumentException
     */
    public function testMovieToJsonWithEmptyMovieThrowsException()
    {
        $this->movieToJson(new Movie(new MovieBuilder("invalid imdbid")));
    }

    public function testMovieToJsonWithOneMovieReturnOneArrayWithSevenFieldsRepresentingTheMovie()
    {
        $this->assertEquals(7, count($this->movieToJson($this->sampleMovie)));
        $this->assertEquals([
            "imdbid" => $this->sampleMovie->getId(),
            "title" => $this->sampleMovie->getTitle(),
            "genre" => $this->sampleMovie->getGenre()->getValue(),
            "year" => $this->sampleMovie->getYear(),
            "rating" => $this->sampleMovie->getRating(),
            "directors" => $this->personsToArray($this->sampleMovie->getDirectors()),
            "stars" => $this->personsToArray($this->sampleMovie->getStars())
        ], $this->movieToJson($this->sampleMovie));

    }

    /**
     * @expectedException     \InvalidArgumentException
     */
    public function testMovieFromJsonWithEmptyArrayThrowsException()
    {
        $this->movieFromJson([]);
    }

    /**
     * @expectedException     \InvalidArgumentException
     */
    public function testMovieFromJsonWithInvalidArrayMovieFormatThrowsException()
    {
        $this->movieFromJson(["thiskey" => "doesntexist"]);
    }

    public function testUpdateMovieFromJsonWithEmptyArrayReturnsTheOriginalMovie()
    {
        $this->assertEquals($this->sampleMovie, $this->updateMovieFromJson([], $this->sampleMovie));
    }

    public function testUpdateMovieFromJsonWithImdbIdUpdatedDoesntUpdateTheOriginalMovie()
    {
        $updateMovie = $this->updateMovieFromJson(["imdbid" => "tt9999999"], $this->sampleMovie);
        $this->assertEquals($this->sampleMovie, $updateMovie);
    }

    public function testUpdateMovieFromJsonWithGenreUpdatedAppliesThatUpdatedToTheOriginalMovie()
    {
        $updateMovie = $this->updateMovieFromJson(["genre" => "DRAMA"], $this->sampleMovie);
        $this->assertEquals(new Genre(Genre::DRAMA), $updateMovie->getGenre());
    }

    public function testUpdateMovieFromJsonWithYearUpdatedAppliesThatUpdatedToTheOriginalMovie()
    {
        $updateMovie = $this->updateMovieFromJson(["year" => 2015], $this->sampleMovie);
        $this->assertEquals(2015, $updateMovie->getYear());
    }

    public function testUpdateMovieFromJsonWithRatingUpdatedAppliesThatUpdatedToTheOriginalMovie()
    {
        $updateMovie = $this->updateMovieFromJson(["rating" => 8], $this->sampleMovie);
        $this->assertEquals(8, $updateMovie->getRating());
    }

    public function testUpdateMovieFromJsonWithDirectorsUpdatedAppliesThatUpdatedToTheOriginalMovie()
    {
        $newDirectors = ["directors" => [
            ["lastName" => "A", "firstName" => "B"],
            ["lastName" => "C", "firstName" => "D"]
        ]
        ];
        $updateMovie = $this->updateMovieFromJson($newDirectors, $this->sampleMovie);
        $this->assertEquals($newDirectors["directors"], $this->personsToArray($updateMovie->getDirectors()));
    }

    public function testUpdateMovieFromJsonWithStarsUpdatedAppliesThatUpdatedToTheOriginalMovie()
    {
        $newDirectors = ["stars" => [
            ["lastName" => "A", "firstName" => "B"],
            ["lastName" => "C", "firstName" => "D"]
        ]
        ];
        $updateMovie = $this->updateMovieFromJson($newDirectors, $this->sampleMovie);
        $this->assertEquals($newDirectors["stars"], $this->personsToArray($updateMovie->getStars()));
    }

}
