<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use PHPUnit\Framework\TestCase;

class MovieTest extends TestCase
{
    private $movie;
    private $anotherMovie;

    public function setUp()
    {
        $movieBuilder = (new MovieBuilder("tt1234567"))
            ->title("a title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2017)
            ->rating(10)
            ->directors(new PersonCollection(new Person("Bocchieri", "Massimiliano")))
            ->stars(new PersonCollection(new Person("Bocchieri", "Massimiliano")));
        $this->movie = $movieBuilder->build();

        $movieBuilder = (new MovieBuilder("tt1234568"))
            ->title("a title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2017)
            ->rating(10)
            ->directors(new PersonCollection(new Person("Bocchieri", "Massimiliano")))
            ->stars(new PersonCollection(new Person("Bocchieri", "Massimiliano")));

        $this->anotherMoviee = $movieBuilder->build();

    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCannotCreateWithInvalidImdbidThrowsException()
    {
        new Movie((new MovieBuilder("thisisinvalid"))->build());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCannotCreateWithIncompleteFieldsThrowsException()
    {
        new Movie((
        new MovieBuilder("tt1234567"))
            ->title("a title")
            ->build()
        );
    }

    public function testCanCreateAMovie()
    {
        $this->assertNotNull($this->movie);
        $this->assertInstanceOf(Movie::class, $this->movie);
    }

    public function testMovieIsSameAsItself()
    {
        $this->assertTrue($this->movie->isSame($this->movie));
    }

    public function testDifferentMoviesAreNotTheSame()
    {
        $this->assertFalse($this->movie->isSame($this->anotherMovie));
    }

    public function testImdbIdIsSet()
    {
        $this->assertEquals("tt1234567", $this->movie->getId());
    }

    public function testTitleIsSet()
    {
        $this->assertEquals("a title", $this->movie->getTitle());
    }

    public function testGenreIsSet()
    {
        $this->assertEquals(Genre::ACTION, $this->movie->getGenre());
    }

    public function testYearIsSet()
    {
        $this->assertEquals(2017, $this->movie->getYear());
    }

    public function testRatingIsSet()
    {
        $this->assertEquals(10, $this->movie->getRating());
    }

    public function testDirectorsAreSet()
    {
        $this->assertEquals(new PersonCollection(new Person("Bocchieri", "Massimiliano")),
            $this->movie->getDirectors());
    }

    public function testStarsAreSet()
    {
        $this->assertEquals(new PersonCollection(new Person("Bocchieri", "Massimiliano")),
            $this->movie->getStars());
    }

}
