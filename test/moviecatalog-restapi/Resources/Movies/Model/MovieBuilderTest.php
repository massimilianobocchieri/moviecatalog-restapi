<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use PHPUnit\Framework\TestCase;

class MovieBuilderTest extends TestCase
{
    private $movieBuilder;

    public function setUp()
    {
        $this->movieBuilder = (new MovieBuilder("tt1234567"))
            ->title("a title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2018)
            ->rating(7)
            ->directors(new PersonCollection(new Person("Burton", "Tim")))
            ->stars(new PersonCollection(new Person("Tear", "Paul")));
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testBuildingWithInvalidImdbidThrowsException()
    {
        (new MovieBuilder("invalid imdbid"))->build();
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testBuildingWithIncompleteFieldsThrowsException()
    {
        (new MovieBuilder("tt1234567"))
            ->title("only title is present")
            ->build();
    }

    public function testCanCreateMovieBuilder()
    {
        $this->assertNotNull($this->movieBuilder);
        $this->assertInstanceOf(MovieBuilder::class, $this->movieBuilder);
    }

    public function testIfSetTitleTitleIsSetAndBuilderInstanceReturned()
    {
        $titleIsSetReturn = $this->movieBuilder->title("a title");
        $this->assertEquals("a title", $this->movieBuilder->title);
        $this->assertEquals($this->movieBuilder, $titleIsSetReturn);
    }

    public function testIfSetGenreGenreIsSetAndBuilderInstanceReturned()
    {
        $genreIsSetReturn = $this->movieBuilder->genre(new Genre(Genre::ACTION));
        $this->assertEquals(Genre::ACTION, $this->movieBuilder->genre);
        $this->assertEquals($this->movieBuilder, $genreIsSetReturn);
    }

    public function testIfSetYearYearIsSetAndBuilderInstanceReturned()
    {
        $genreIsSetReturn = $this->movieBuilder->year(2017);
        $this->assertEquals(2017, $this->movieBuilder->year);
        $this->assertEquals($this->movieBuilder, $genreIsSetReturn);
    }

    public function testIfSetRatingRatingIsSetAndBuilderInstanceReturned()
    {
        $yearIsSetReturn = $this->movieBuilder->rating(10);
        $this->assertEquals(10, $this->movieBuilder->rating);
        $this->assertEquals($this->movieBuilder, $yearIsSetReturn);
    }

    public function testIfSetDirectorsDirectorsIsSetAndBuilderInstanceReturned()
    {
        $directorsIsSetReturn = $this->movieBuilder->directors(
            new PersonCollection(
                new Person("Bocchieri", "Massimiliano")));

        $this->assertEquals(new PersonCollection(
            new Person("Bocchieri", "Massimiliano")),
            $this->movieBuilder->directors);
        $this->assertEquals($this->movieBuilder, $directorsIsSetReturn);
    }

    public function testIfSetStarsStarsIsSetAndBuilderInstanceReturned()
    {
        $starsIsSetReturn = $this->movieBuilder->stars(
            new PersonCollection(
                new Person("Bocchieri", "Massimiliano")));

        $this->assertEquals(new PersonCollection(
            new Person("Bocchieri", "Massimiliano")),
            $this->movieBuilder->stars);
        $this->assertEquals($this->movieBuilder, $starsIsSetReturn);
    }

    public function testAfterBuildAMovieIsCreated()
    {
        $aMovie = $this->movieBuilder->build();
        $this->assertInstanceOf(Movie::class, $aMovie);
    }

}
