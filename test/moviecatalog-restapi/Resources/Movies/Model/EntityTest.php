<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Model;

use PHPUnit\Framework\TestCase;

class EntityTest extends TestCase
{
    private $entity;

    public function setUp()
    {
        $this->entity = new Entity("thisisanid");
    }

    public function testCanCreateAnEntity()
    {
        $this->assertNotNull($this->entity);
        $this->assertInstanceOf(Entity::class, $this->entity);
    }

    public function testIdIsSet()
    {
        $this->assertEquals("thisisanid", $this->entity->getId());
    }

    public function testTwoEntitiesWithSameIdAreTheSame()
    {
        $this->assertEquals($this->entity, new Entity("thisisanid"));
    }

    public function testCanCloneAnEntity()
    {
        $this->assertNotNull(clone $this->entity);
    }

    /**
     * @expectedException     \ArgumentCountError
     */
    public function testCannotCreateAnEntityWithoutId()
    {
        $this->assertEquals($this->entity, new Entity());
    }

}
