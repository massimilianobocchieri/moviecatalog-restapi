<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Doubles;

use MovieCatalogRestApi\Resources\Movies\Model\Genre;
use MovieCatalogRestApi\Resources\Movies\Model\MovieBuilder;
use MovieCatalogRestApi\Resources\Movies\Model\MovieCollection;
use MovieCatalogRestApi\Resources\Movies\Model\Person;
use MovieCatalogRestApi\Resources\Movies\Model\PersonCollection;
use PHPUnit\Framework\TestCase;

class InMemoryMovieCatalogRepositoryTest extends TestCase
{
    private $repository;
    private $sampleMovie;

    public function setUp()
    {
        $this->repository = new InMemoryMovieCatalogRepository();
        $this->sampleMovie = (new MovieBuilder("tt1234567"))
            ->title(" a title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2017)
            ->rating(10)
            ->directors(new PersonCollection(new Person("Burton", "Bob")))
            ->stars(new PersonCollection(new Person("Carty", "Tim")))
            ->build();

    }

    public function testCanCreate()
    {
        $this->assertInstanceOf(InMemoryMovieCatalogRepository::class, $this->repository);
    }

    public function testSaveOneMovieTheMovieGetsSaved()
    {
        $this->repository->save($this->sampleMovie);
        $this->assertEquals($this->sampleMovie, $this->repository->findMovieByImdbId($this->sampleMovie->getId()));
    }

    public function testSaveOneMovieTheMovieGetsReturned()
    {
        $this->assertEquals($this->sampleMovie, $this->repository->save($this->sampleMovie));
    }

    public function testDeleteOneExistentMovieTheMovieGetsDeleted()
    {
        $this->repository->save($this->sampleMovie);
        $this->assertEquals(1, count($this->repository->findMovieByImdbId($this->sampleMovie->getId())));
        $this->repository->delete($this->sampleMovie);
        $this->assertNull($this->repository->findMovieByImdbId($this->sampleMovie->getId()));
    }

    public function testMergeIfMovieDoesnExistsNullIsReturned()
    {
        $this->assertNull($this->repository->merge($this->sampleMovie, $this->sampleMovie->getId()));
    }

    public function testMergeIfMovieExistsItIsReturned()
    {
        $updatedMovie = clone $this->sampleMovie;
        $updatedMovie->setRating(9);
        $this->repository->save($this->sampleMovie);
        $foundMovie = $this->repository->merge($updatedMovie, $updatedMovie->getId());
        $this->assertEquals($updatedMovie, $foundMovie);
        $this->assertEquals(9, $foundMovie->getRating());
    }

    public function testFindAllMovieTitlesWithEmptyRepositoryReturnsEmptyArray()
    {
        $this->assertEquals([], $this->repository->findAllMoviesTitle());
    }

    public function testFindAllMovieTitlesWithOneMovieInRepositoryReturnsItsTitle()
    {
        $this->repository->save($this->sampleMovie);
        $this->assertEquals([$this->sampleMovie->getTitle()], $this->repository->findAllMoviesTitle());
    }

    public function testFindMoviesTitleByGenreWithNoMovieHavingADifferentGenreReturnsEmptyArray()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([], $this->repository->findMoviesTitleByGenre(new Genre(Genre::ADVENTURE)));
    }

    public function testFindMoviesTitleByGenreWithEmptyRepositoryReturnsEmptyArray()
    {
        $this->assertEquals([], $this->repository->findMoviesTitleByGenre(new Genre(Genre::ADVENTURE)));
    }

    public function testFindMoviesTitleByGenreWithOneMovieOfThatGenreReturnsThatMovieTitle()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([$this->sampleMovie->getTitle()], $this->repository->findMoviesTitleByGenre($this->sampleMovie->getGenre()));
    }

    public function testfindMoviesTitleByDirectorWithEmptyRepositoryReturnsEmptyArray()
    {
        $this->assertEquals([],
            $this->repository->findMoviesTitleByDirector(new Person("Burton", "Bob")));
    }

    public function testfindMoviesTitleByDirectorNotContainedInAnyMovieReturnsEmptyArray()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([],
            $this->repository->findMoviesTitleByDirector(new Person("dunnou", "boh")));
    }

    public function testfindMoviesTitleByDirectorContainedInAMovieReturnsThatMovieTitle()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([$this->sampleMovie->getTitle()],
            $this->repository->findMoviesTitleByDirector($this->sampleMovie->getDirectors()[0]));
    }

    public function testfindMoviesTitleByStarWithEmptyRepositoryReturnsEmptyArray()
    {
        $this->assertEquals([],
            $this->repository->findMoviesTitleByStar(new Person("Burton", "Bob")));
    }

    public function testfindMoviesTitleByStarNotContainedInAnyMovieReturnsEmptyArray()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([],
            $this->repository->findMoviesTitleByStar(new Person("dunnou", "boh")));
    }

    public function testfindMoviesTitleByStarContainedInAMovieReturnsThatMovieTitle()
    {
        $this->repository->save($this->sampleMovie);

        $this->assertEquals([$this->sampleMovie->getTitle()],
            $this->repository->findMoviesTitleByStar($this->sampleMovie->getStars()[0]));
    }

    public function testFindAllMoviesIfEmptyReturnsEmptyCollection()
    {
        $repository = new InMemoryMovieCatalogRepository();
        $this->assertEquals(new MovieCollection(), $repository->findAllMovies());
    }

    public function testFindAllMoviesAfterInsertingOneMovieReturnOneElement()
    {
        $repository = new InMemoryMovieCatalogRepository();
        $repository->save($this->sampleMovie);
        $this->assertEquals(1, count($repository->findAllMovies()));
    }

    public function testFindAllMoviesAfterInsertingTwoMoviesReturnTwoElements()
    {
        $repository = new InMemoryMovieCatalogRepository();
        $repository->save($this->sampleMovie);
        $repository->save($this->sampleMovie);

        $this->assertEquals(2, count($repository->findAllMovies()));
    }

    public function testWhenRepositoryIsEmptyFindMovieByImdbIdReturnsNull()
    {
        $this->assertNull($this->repository->findMovieByImdbId("tt1234567"));
    }

    public function testFindMovieByImdbIdWithNotExistentIdReturnsNull()
    {
        $this->repository->save($this->sampleMovie);
        $this->assertNull($this->repository->findMovieByImdbId("doesntexist"));
    }

    public function testFindMovieByImdbIdWithExistentIdReturnsOneMovie()
    {
        $this->repository->save($this->sampleMovie);
        $this->assertEquals(1, count($this->repository->findMovieByImdbId($this->sampleMovie->getId())));
        $this->assertEquals($this->sampleMovie, $this->repository->findMovieByImdbId($this->sampleMovie->getId()));
    }


}
