<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Mockery;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;

class ListMovieCatalogControllerTest extends AbstractMovieCatalogControllerTest
{
    public function setUp()
    {
        parent::setUp();

        $this->controller = new ListMovieCatalogController($this->container);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * @expectedException \ArgumentCountError
     */
    public function testCannotCreateWithoutContainer()
    {
        $this->assertInstanceOf(ListMovieCatalogController::class, new ListMovieCatalogController());
    }

    public function testCanCreate()
    {
        $this->assertInstanceOf(ListMovieCatalogController::class, $this->controller);
    }

    public function testCanInvokeController()
    {
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, []);

        $this->assertInstanceOf(ResponseInterface::class, $response);
    }

    public function testResponseHasOkStatus()
    {
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, []);

        $this->assertTrue($response->isOk());
    }

    public function testResponseHasBody()
    {
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, []);

        $this->assertNotNull((string)$response->getBody());
    }


    protected function defineServiceMockBehaviour(): void
    {
        $this->serviceMock->allows()->getMovies()
            ->andReturns($this->testSetup->getSampleMovies());
    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new ListMovieCatalogController($container);
    }
}
