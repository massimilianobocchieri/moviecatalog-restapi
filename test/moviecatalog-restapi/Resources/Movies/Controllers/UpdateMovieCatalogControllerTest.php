<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Infrastructure\Exceptions\DBException;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Container\ContainerInterface;

class UpdateMovieCatalogControllerTest extends AbstractMovieCatalogControllerTest
{
    use MovieJsonMapper;

    private $updatedMovie;

    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNeitherImdbidNorMovieUpdateInfosPassedExceptionIsThrown()
    {
        ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNoImdbidIsPassedExceptionIsThrown()
    {
        $requestWithMovieInfo = $this->defaultRequest->withAttribute('movie', $this->updatedMovie);
        ($this->controller)($requestWithMovieInfo, $this->defaultResponse, []);
    }

    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNoMovieUpdateInfoIsPassedExceptionIsThrown()
    {
        ($this->controller)($this->defaultRequest, $this->defaultResponse, ["imdbid" => "ttsomething"]);
    }

    public function testWhenImdbidIsPresentAndMovieUpdateInfoIsPassedOKAndUpdatedMovieInfoAreReturnedByResponse()
    {
        $imdbid = $this->testSetup->getSampleMovie()->getid();
        $requestWithUpdatedInfo = $this->defaultRequest->withAttribute('movie', $this->updatedMovie);

        $response = ($this->controller)($requestWithUpdatedInfo, $this->defaultResponse, ["imdbid" => $imdbid]);

        $this->assertEquals(200, $response->getStatuscode());
        $this->assertEquals(
            $this->movieToJson($this->updatedMovie),
            json_decode((string)$response->getBody(), true));
    }

    protected function defineServiceMockBehaviour(): void
    {
        $this->updatedMovie = $this->testSetup->getSampleMovie();
        $this->updatedMovie->setTitle("updated title");

        $this->serviceMock->allows()->updateMovie(
            $this->updatedMovie,
            $this->updatedMovie->getId())
            ->andReturns($this->updatedMovie);
   }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new UpdateMovieCatalogController($container);
    }
}
