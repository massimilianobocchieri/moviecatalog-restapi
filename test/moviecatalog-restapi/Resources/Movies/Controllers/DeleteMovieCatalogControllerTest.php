<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Psr\Container\ContainerInterface;

class DeleteMovieCatalogControllerTest extends AbstractMovieCatalogControllerTest
{
    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNoImdbidIsPassedExceptionIsThrown()
    {
        ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
    }

    public function testWhenNoImdbidIsPresentFourOuFourIsReturnedByResponse()
    {
        $imdbid = "imdbnotexisting";
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, ["imdbid" => $imdbid]);
        $this->assertEquals(404, $response->getStatuscode());
        $this->assertEquals("{\"message\":\"movie {$imdbid} not found\"}", (string)$response->getBody());
    }

    public function testWhenImdbidIsPresentTwoOuFourAndEmptyBodyAreReturnedByResponse()
    {
        $imdbid = $this->testSetup->getSampleMovie()->getid(); //Demeter I am sorry but was useful...
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, ["imdbid" => $imdbid]);
        $this->assertEquals(204, $response->getStatuscode());
        $this->assertEquals('null', (string)$response->getBody());
    }

    protected function defineServiceMockBehaviour(): void
    {
        $this->serviceMock->allows()->deleteMovie()
            ->andReturns(null);

        $this->serviceMock->allows()->deleteMovie("imdbnotexisting")
            ->andReturns(null);

        $this->serviceMock->allows()->deleteMovie($this->testSetup->getSampleMovie()->getId())
            ->andReturns($this->testSetup->getSampleMovie());
    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new DeleteMovieCatalogController($container);
    }
}
