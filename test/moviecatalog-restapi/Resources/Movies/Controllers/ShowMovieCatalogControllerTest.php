<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Container\ContainerInterface;

class ShowMovieCatalogControllerTest extends AbstractMovieCatalogControllerTest
{
    use MovieJsonMapper;

    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNoImdbidIsPassedExceptionIsThrown()
    {
        ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
    }

    public function testWhenNoImdbidIsPresentFourOuFourIsReturnedByResponse()
    {
        $imdbid = "imdbnotexisting";
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, ["imdbid" => $imdbid]);
        $this->assertEquals(404, $response->getStatuscode());
        $this->assertEquals("{\"message\":\"movie {$imdbid} not found\"}", (string)$response->getBody());
    }

    public function testWhenImdbidIsPresentOKAndMovieDetailsAreReturnedByResponse()
    {
        $imdbid = $this->testSetup->getSampleMovie()->getid();
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, ["imdbid" => $imdbid]);
        $this->assertEquals(200, $response->getStatuscode());
        $this->assertEquals(
            $this->movieToJson($this->testSetup->getSampleMovie()),
            json_decode((string)$response->getBody(), true));
    }

    protected function defineServiceMockBehaviour(): void
    {
        $this->serviceMock->allows()->getMovieByImdbId("imdbnotexisting")
            ->andReturns(null);

        $this->serviceMock->allows()->getMovieByImdbId($this->testSetup->getSampleMovie()->getId())
            ->andReturns($this->testSetup->getSampleMovie());
    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new ShowMovieCatalogController($container);
    }
}
