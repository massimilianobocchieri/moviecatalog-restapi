<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Mockery;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

abstract class AbstractMovieCatalogControllerTest extends TestCase
{

    protected $controller;
    protected $container;

    protected $defaultRequest;
    protected $defaultResponse;
    protected $testSetup;
    protected $serviceMock;

    public function setUp()
    {
        $this->testSetup = new TestSetup();

        $this->defaultRequest = Request::createFromGlobals([]);
        $this->defaultResponse = new Response();

        $this->serviceMock = Mockery::mock(MovieCatalogService::class);

        $this->defineServiceMockBehaviour();

        $this->container = new DependencyInjectionContainer();
        $serviceMock = $this->serviceMock;
        $this->container->set('movieCatalogService', function ($container) use ($serviceMock) {
            return $serviceMock;
        });

        $this->controller = $this->defineController($this->container);

    }

    protected abstract function defineServiceMockBehaviour(): void;

    protected abstract function defineController(ContainerInterface $container): AbstractController;

    public function tearDown()
    {
        Mockery::close();
    }

}