<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use MovieCatalogRestApi\Infrastructure\Exceptions\DBException;
use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use Psr\Container\ContainerInterface;

class CreateMovieCatalogControllerTest extends AbstractMovieCatalogControllerTest
{
    use MovieJsonMapper;

    /**
     * @expectedException \InvalidArgumentException
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenNoMovieAttributeIsPassedExceptionIsThrown()
    {
        ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
    }

    public function testWhenSingleMovieIsInsertedOKAndThatdMovieInfoAreReturnedByResponse()
    {
        $requestWithMovieInfo = $this->defaultRequest->withAttribute('movie', $this->testSetup->getSampleMovie());

        $response = ($this->controller)($requestWithMovieInfo, $this->defaultResponse, []);

        $this->assertEquals(201, $response->getStatuscode());
        $this->assertEquals(
            $this->movieToJson($this->testSetup->getSampleMovie()),
            json_decode((string)$response->getBody(), true));
    }

    public function testWhenSingleMovieIsNotInsertedFiveHundredAndErrorMessageAreReturnedByResponse()
    {
        $requestWithMovieInfo = $this->defaultRequest->withAttribute('movie', $this->testSetup->getAnotherSampleMovie());

        $response = ($this->controller)($requestWithMovieInfo, $this->defaultResponse, []);

        $this->assertEquals(500, $response->getStatuscode());
        $this->assertEquals(
            ["message" => "an error occurred while inserting the movie, operation aborted"],
            json_decode((string)$response->getBody(), true));
    }

    public function testWhenMultipleMovieaAreInsertedOKAndThosedMoviesInfoAreReturnedByResponse()
    {
        $requestWithMovieInfo = $this->defaultRequest->withAttribute('movies', $this->testSetup->getSampleMovies());

        $response = ($this->controller)($requestWithMovieInfo, $this->defaultResponse, []);

        $this->assertEquals(201, $response->getStatuscode());
        $this->assertEquals(
            $this->moviesToJson($this->testSetup->getSampleMovies()),
            json_decode((string)$response->getBody(), true));
    }

    public function testWhenMultipleMovieAreNotInsertedFiveHundredAndErrorMessageAreReturnedByResponse()
    {
        $requestWithMovieInfo = $this->defaultRequest->withAttribute('movies', $this->testSetup->getAnotherSampleMovies());

        $response = ($this->controller)($requestWithMovieInfo, $this->defaultResponse, []);

        $this->assertEquals(500, $response->getStatuscode());
        $this->assertEquals(
            ["message" => "an error occurred while inserting the movies, operation aborted"],
            json_decode((string)$response->getBody(), true));
    }

    protected function defineServiceMockBehaviour(): void
    {
        $this->serviceMock->allows()->createMovie()
            ->with(\Hamcrest\Matchers::equalTo($this->testSetup->getSampleMovie()))
            ->andReturns($this->testSetup->getSampleMovie());

        $this->serviceMock->allows()->createMovie()
            ->with(\Hamcrest\Matchers::equalTo($this->testSetup->getAnotherSampleMovie()))
            ->andThrow(DBException::class);

        $this->serviceMock->allows()->createMovies()
            ->with(\Hamcrest\Matchers::equalTo($this->testSetup->getSampleMovies()))
            ->andReturns($this->testSetup->getSampleMovies());

        $this->serviceMock->allows()->createMovies()
            ->with(\Hamcrest\Matchers::equalTo($this->testSetup->getAnotherSampleMovies()))
            ->andThrow(DBException::class);
    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new CreateMovieCatalogController($container);
    }
}
