<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Psr\Container\ContainerInterface;

class FourOuFourControllerTest extends AbstractMovieCatalogControllerTest
{
    public function testReturnsAlwaysFourOuFourAndNotoundMessage()
    {
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals("{\"message\":\"NotFound\"}", (string)$response->getBody());
    }

    protected function defineServiceMockBehaviour(): void
    {
        //no service needed
    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new FourOuFourController($container);
    }
}
