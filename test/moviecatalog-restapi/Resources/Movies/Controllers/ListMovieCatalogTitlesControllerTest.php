<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Controllers;

use Psr\Container\ContainerInterface;

class ListMovieCatalogTitlesControllerTest extends AbstractMovieCatalogControllerTest
{

    /**
     * @expectedException \TypeError
     *
     * Precondition Controllers rely on their related Filters to validate and preorganize input
     *
     */
    public function testWhenFilterAttributeDoesntExistExceptionIsThrown()
    {
        $response = ($this->controller)($this->defaultRequest, $this->defaultResponse, []);
    }

    public function testWhenFilterIsEmptyReturnsAllMovieTitles()
    {
        $requestWithFilter = $this->defaultRequest->withAttribute('filter', []);
        $response = ($this->controller)($requestWithFilter, $this->defaultResponse, []);

        $this->assertNotNull($response);
        $this->assertTrue($response->isOk());
        $this->assertNotNull((string)$response->getBody());
    }

    public function testWhenNoTitlesGetResponsecontainsEmptyArray()
    {
        $requestWithFilter = $this->defaultRequest->withAttribute('filter', ["filterBy" => "genre", "filterValue" => "DOESNTEXIST"]);
        $response = ($this->controller)($requestWithFilter, $this->defaultResponse, []);

        $this->assertNotNull($response);
        $this->assertTrue($response->isOk());
        $this->assertEquals("[]", (string)$response->getBody());
    }

    public function testWhenFilterByExistingGenreReturnsFilteredMovieTitles()
    {
        $requestWithFilter = $this->defaultRequest->withAttribute('filter', ["filterBy" => "genre", "filterValue" => "ADVENTURE"]);
        $response = ($this->controller)($requestWithFilter, $this->defaultResponse, []);

        $this->assertNotNull($response);
        $this->assertTrue($response->isOk());
        $this->assertStringStartsWith(
            "[\"{$this->testSetup->getAnotherSampleMovie()->getTitle()}\"]",
            (string)$response->getBody());
    }

    protected function defineServiceMockBehaviour(): void
    {
        $this->serviceMock->allows()->getMoviesTitle()
            ->andReturns([$this->testSetup->getSampleMovie()->getTitle()]);

        $this->serviceMock->allows()->getMoviesTitle([])
            ->andReturns([$this->testSetup->getSampleMovie()->getTitle()]);

        $this->serviceMock->allows()->getMoviesTitle(["filterBy" => "genre", "filterValue" => "ADVENTURE"])
            ->andReturns([$this->testSetup->getAnotherSampleMovie()->getTitle()]);

        $this->serviceMock->allows()->getMoviesTitle(["filterBy" => "genre", "filterValue" => "DOESNTEXIST"])
            ->andReturns([]);


    }

    protected function defineController(ContainerInterface $container): AbstractController
    {
        return new ListMovieCatalogTitlesController($container);
    }
}
