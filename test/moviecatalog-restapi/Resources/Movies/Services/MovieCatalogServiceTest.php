<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Services;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Doubles\InMemoryMovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Model\{
    Genre, MovieBuilder, MovieCollection, Person, PersonCollection
};
use PHPUnit\Framework\TestCase;

class MovieCatalogServiceTest extends TestCase
{
    private $service;
    private $movieSample;
    private $anotherMovieSample;


    public function setUp()
    {
        $this->movieSample = (new MovieBuilder("tt1234567"))
            ->title(" a title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2017)
            ->rating(10)
            ->directors(new PersonCollection(new Person("Burton", "Bob")))
            ->stars(new PersonCollection(new Person("Carty", "Tim")))
            ->build();

        $this->anotherMovieSample = (new MovieBuilder("tt1234568"))
            ->title(" another title")
            ->genre(new Genre(Genre::ACTION))
            ->year(2018)
            ->rating(5)
            ->directors(new PersonCollection(new Person("Merry", "Larry")))
            ->stars(new PersonCollection(new Person("Plateau", "Janvier")))
            ->build();


        $this->service = new MovieCatalogService(new DependencyInjectionContainer(), new InMemoryMovieCatalogRepository(new MovieCollection($this->movieSample)));
    }

    public function testGetMoviesWithEmptyRepositoryReturnsEmptyMovieCollection()
    {
        $this->assertEquals(new MovieCollection(), (new MovieCatalogService(new DependencyInjectionContainer(), new InMemoryMovieCatalogRepository()))->getMovies());
    }

    public function testGetMoviesWithOneMovieReturnsAMovieCollectionWithThatMovie()
    {
        $this->assertEquals(new MovieCollection($this->movieSample), $this->service->getMovies());
    }

    public function testGetMoviesTitleWithEmptyFilterReturnsAllTitles()
    {
        $this->assertEquals(count($this->service->getMovies()->toArray()), count($this->service->getMoviesTitle()));
    }

    public function testGetMoviesTitleWithGenreFilterValueNotValidingReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(["filterBy" => "genre", "filterValue" => "INVALIDGENREVALUE"]));
    }

    public function testGetMoviesTitleWithGenreFilterNotPresentReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(["filterBy" => "genre", "filterValue" => "ADVENTURE"]));
    }

    public function testGetMoviesTitleWithGenreFilterExistingReturnsOneElement()
    {
        $this->assertEquals([$this->movieSample->getTitle()], $this->service->getMoviesTitle(["filterBy" => "genre", "filterValue" => $this->movieSample->getGenre()->getValue()]));
    }

    public function testGetMoviesTitleWithDirectorFilterValueNotValidingReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(["filterBy" => "director", "filterValue" => "InvalidStructure"]));
    }

    public function testGetMoviesTitleWithDirectorFilterValueNotPresentReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(
            ["filterBy" => "director", "filterValue" =>
                ["lastName" => "Not", "firstName" => "there"]]));
    }

    public function testGetMoviesTitleWithDirectorFilterValuePresentReturnsAnElement()
    {
        $this->assertEquals([$this->movieSample->getTitle()], $this->service->getMoviesTitle(
            ["filterBy" => "director", "filterValue" =>
                ["lastName" => "Burton", "firstName" => "Bob"]]));
    }

    public function testGetMoviesTitleWithStarFilterValueNotValidingReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(["filterBy" => "star", "filterValue" => "InvalidStructure"]));
    }

    public function testGetMoviesTitleWithStarFilterValueNotPresentReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(
            ["filterBy" => "star", "filterValue" =>
                ["lastName" => "Not", "firstName" => "there"]]));
    }

    public function testGetMoviesTitleWithStarFilterValuePresentReturnsAnElement()
    {
        $this->assertEquals([$this->movieSample->getTitle()], $this->service->getMoviesTitle(
            ["filterBy" => "star", "filterValue" =>
                ["lastName" => "Carty", "firstName" => "Tim"]]));
    }

    public function testGetMoviesTitleWithInvalidFilterReturnsEmptyArray()
    {
        $this->assertEquals([], $this->service->getMoviesTitle(
            ["filterBy" => "wrong filterType"]));
    }

    /**
     * @expectedException     ArgumentCountError
     */
    public function testGetMovieByImdbIdWithNoArgumentThrowsInvalidArgumentException()
    {
        $this->service->getMovieByImdbId();
    }

    /**
     * @expectedException     TypeError
     */
    public function testGetMovieByImdbIdWithNullIdThrowsInvalidArgumentException()
    {
        $this->service->getMovieByImdbId(null);
    }

    public function testGetMovieByImdbIdWithIdNotInRepositoryReturnsNull()
    {
        $this->assertNull($this->service->getMovieByImdbId("notExisting"));
    }

    public function testGetMovieByImdbIdWithExistingIdReturnsTheMovie()
    {
        $this->assertEquals($this->movieSample, $this->service->getMovieByImdbId($this->movieSample->getId()));
    }

    public function testCreateMovieIfInsertedThenReturned()
    {
        $this->assertEquals($this->anotherMovieSample, $this->service->createMovie($this->anotherMovieSample));
    }

    public function testCreateMovieIfAlreadyPresentThenReturned()
    {
        $this->assertEquals($this->movieSample, $this->service->createMovie($this->movieSample));
    }


    public function testUpdateMovieIfNotPresentThenNullReturned()
    {
        $this->assertNull($this->service->updateMovie($this->anotherMovieSample, $this->anotherMovieSample->getId()));
    }

    public function testUpdateMovieWithMoviePresentThenReturnThatMovie()
    {
        $this->service->createMovie($this->anotherMovieSample);

        $this->assertEquals($this->anotherMovieSample, $this->service->updateMovie($this->anotherMovieSample, $this->anotherMovieSample->getId()));
    }

    public function testUpdateMovieWithFieldsUpdatedIfMoviePresentThenReturnThatMovieWithFieldsUpdatedAccordingly()
    {
        $this->service->createMovie($this->anotherMovieSample);

        $updatedMovie = clone $this->anotherMovieSample;
        $updatedMovie->setTitle("updated title");
        $updatedMovie->setGenre(new Genre(Genre::CARTOON));
        $updatedMovie->setYear(2099);
        $updatedMovie->setYear(3);
        $updatedMovie->setDirectors(new PersonCollection(new Person("UpdatedDirectorLN", "UpdatedDirectorFN")));
        $updatedMovie->setDirectors(new PersonCollection(new Person("UpdatedStarLN", "UpdatedStarFN")));

        $this->assertEquals($updatedMovie, $this->service->updateMovie($updatedMovie, $updatedMovie->getId()));
    }

}
