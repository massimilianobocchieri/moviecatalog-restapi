<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Validators;

use Mockery;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Filters\UpdateMovieCatalogFilter;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

class UpdateMovieCatalogFilterTest extends TestCase
{
    private $containerMock;
    private $updateMovieFilter;
    private $serviceMock;
    private $sampleMovie;
    private $updatedMovie;
    private $notExistingImdbId;


    public function setUp()
    {
        $testSetup = new TestSetup();
        $this->sampleMovie = $testSetup->getSampleMovie();

        $this->notExistingImdbId = "tt9999999";

        $this->serviceMock = Mockery::mock(MovieCatalogService::class);

        $this->serviceMock->allows()->getMovieByImdbId(TestSetup::getSampleImDbId())
            ->andReturns($this->sampleMovie);

        $this->serviceMock->allows()->getMovieByImdbId($this->notExistingImdbId)
            ->andReturns(null);

        $movieToUpdate = $testSetup->getAnotherSampleMovie();
        $movieToUpdate->setTitle(" updated title");
        $this->updatedMovie = clone $movieToUpdate;

        $this->serviceMock->allows()->getMovieByImdbId($testSetup->getAnotherSampleMovie()->getId())
            ->andReturns($movieToUpdate);

        $this->containerMock = Mockery::mock(DependencyInjectionContainer::class);
        $this->containerMock->allows()->get('movieCatalogService')->andReturns($this->serviceMock);

        $this->updateMovieFilter = new UpdateMovieCatalogFilter($this->containerMock);
    }

    public function tearDown()
    {
        Mockery::close();
    }

    public function testCanCreate()
    {
        $this->assertNotNull($this->updateMovieFilter);
    }

    public function testWhenReuqestBodyIsNullAndMovieExistsReturnsFoundOriginalMovie()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(
                ["REQUEST_METHOD" => "PUT", "REQUEST_URI" => "/api/movies/" . TestSetup::getSampleImDbId()]))
            ->withParsedBody([]), new Response(), ["imdbid" => TestSetup::getSampleImDbId()]);

        $this->assertNotNull($request->getAttribute('movie'));
        $this->assertEquals($this->sampleMovie, $request->getAttribute('movie'));
    }

    public function testWhenReuqestBodycontainsAfieldUpdatedAndMovieExistsReturnsupdatedMovieMovie()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(
                ["REQUEST_METHOD" => "PUT",
                    "REQUEST_URI" => "/api/movies/" . $this->updatedMovie->getId()
                ])
        )
            ->withParsedBody([]), new Response(), ["imdbid" => $this->updatedMovie->getId()]);

        $this->assertNotNull($request->getAttribute('movie'));
        $this->assertEquals($this->updatedMovie, $request->getAttribute('movie'));
    }

    public function testWhenReuqestBodyNullAndNoMovieExistsReturnsErrorResponseInTheRequestWithFourOuFourStatusCode()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(
                ["REQUEST_METHOD" => "PUT", "REQUEST_URI" => "/api/movies/" . TestSetup::getSampleImDbId()]))
            ->withParsedBody([]), new Response(), ["imdbid" => $this->notExistingImdbId]);

        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(404, $request->getAttribute('responseError')->getStatusCode());

    }

    public function testWhenReuqestBodyContainsMovieImdbidReturnsAnErrorResponseInTheRequestWithFourSixsteenStatusCode()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"]))
            ->withParsedBody(["imdbid" => TestSetup::getSampleImDbId()]),
            new Response(),
            ["imdbid" => TestSetup::getSampleImDbId()]);

        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(416, $request->getAttribute('responseError')->getStatusCode());
    }

    public function testWhenReuqestBodyIsInvalidReturnsAnErrorResponseInTheRequestWithFourSixsteenStatusCode()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"]))
            ->withParsedBody(["genre" => "invalid genre"]), new Response(), ["imdbid" => TestSetup::getSampleImDbId()]);

        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(416, $request->getAttribute('responseError')->getStatusCode());
    }

    public function testWhenMovieIsNotPresentReturnsRequestWithResponseErrorAndFourOuForStatusCode()
    {
        $request = ($this->updateMovieFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"]))
            ->withParsedBody(["title" => "updated title"]), new Response(), ["imdbid" => $this->notExistingImdbId]);

        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(404, $request->getAttribute('responseError')->getStatusCode());
    }

}
