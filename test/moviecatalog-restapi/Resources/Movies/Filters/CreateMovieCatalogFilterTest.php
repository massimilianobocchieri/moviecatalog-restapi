<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Filters;

use Mockery;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Model\MovieCollection;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;
use Slim\{
    Http\Environment, Http\Request, Http\Response
};

class CreateMovieCatalogFilterTest extends TestCase
{
    private $containerMock;
    private $createMovieFilter;
    private $serviceMock;

    public function setUp()
    {
        $testSetup = new TestSetup();
        $this->sampleMovie = $testSetup->getSampleMovie();

        $this->serviceMock = Mockery::mock(MovieCatalogService::class);
        $this->serviceMock->allows()->getMovieByImdbId("tt0120587")
            ->andReturns(null);
        $this->serviceMock->allows()->getMovieByImdbId(TestSetup::getSampleImDbId())
            ->andReturns($this->sampleMovie);
        $this->serviceMock->allows()->getMovieByImdbId($testSetup->getAnotherSampleMovie()->getId())
            ->andReturns($testSetup->getAnotherSampleMovie());

        $this->containerMock = Mockery::mock(DependencyInjectionContainer::class);
        $this->containerMock->allows()->get('movieCatalogService')->andReturns($this->serviceMock);

        $this->createMovieFilter = new CreateMovieCatalogFilter($this->containerMock);
    }

    public function tearDown()
    {
        Mockery::close();
    }


    public function testCanCreate()
    {
        $this->assertNotNull($this->createMovieFilter);
    }

    public function testWhenReuqestBodyIsNullReturnsAnErrorResponseInTheRequest()
    {
        $request = ($this->createMovieFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"]))
            ->withParsedBody([]), new Response(), []);
        $this->assertNotNull($request->getAttribute('responseError'));
    }

    public function testWhenReuqestBodyIsInvalidReturnsAnErrorResponseInTheRequest()
    {
        $request = ($this->createMovieFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies"]))
            ->withParsedBody(["invalidBody"]), new Response(), []);
        $this->assertNotNull($request->getAttribute('responseError'));
    }

    public function testWhenReuqestBodyHasSingleMovieReturnsTheRequestWithTheMovieInjected()
    {
        $this->serviceMock->allows()->getMoviesByIds(["tt0120587", "tt0112573"])
            ->andReturns(new MovieCollection());


        $startingRequest = Request::createFromGlobals([])
            ->withParsedBody(TestSetup::getSampleMovieAsArray());
        $request = ($this->createMovieFilter)($startingRequest, new Response(), []);
        $this->assertNotNull($request->getAttribute('movie'));
        $this->assertEquals((new TestSetup)->getSampleMovie(), $request->getAttribute('movie'));
    }

    public function testWhenReuqestBodyHasMultipleMoviesReturnsTheRequestWithTheMoviesInjected()
    {
        $this->serviceMock->allows()->getMoviesByIds(["tt0120587", "tt0112573"])
            ->andReturns(new MovieCollection());


        $startingRequest = Request::createFromGlobals([])
            ->withParsedBody(TestSetup::getSampleMoviesAsArray());
        $request = ($this->createMovieFilter)($startingRequest, new Response(), []);
        $this->assertNotNull($request->getAttribute('movies'));
        $this->assertInstanceOf(MovieCollection::class, $request->getAttribute('movies'));
    }

    public function testWhenReuqestBodyHasSingleMovieAlreadyPresentReturnsErrorResponseInjected()
    {
        $startingRequest = Request::createFromGlobals([])
            ->withParsedBody(TestSetup::getAnotherSampleMovieAsArray());
        $request = ($this->createMovieFilter)($startingRequest, new Response(), []);
        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(416, $request->getAttribute('responseError')->getStatusCode());
    }

    public function testWhenReuqestBodyHasMultipleMoviesAlreadyPresentReturnsErrorResponseInjected()
    {
        $this->serviceMock->allows()->getMoviesByIds(["tt0120587", "tt0112573"])
            ->andReturns((new TestSetup())->getSampleMovies());

        $startingRequest = Request::createFromGlobals([])
            ->withParsedBody(TestSetup::getSampleMoviesAsArray());
        $request = ($this->createMovieFilter)($startingRequest, new Response(), []);
        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(416, $request->getAttribute('responseError')->getStatusCode());
    }

}
