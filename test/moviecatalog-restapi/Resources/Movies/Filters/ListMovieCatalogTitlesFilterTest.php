<?php
declare(strict_types=1);

namespace MovieCatalogRestApi\Resources\Movies\Filters;

use Mockery;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\TestSetup;
use PHPUnit\Framework\TestCase;
use Slim\{
    Http\Environment, Http\Request, Http\Response
};

class ListMovieCatalogTitlesFilterTest extends TestCase
{
    private $containerMock;
    private $listMovieCatalogTitleFilter;

    public function setUp()
    {
        $testSetup = new TestSetup();
        $this->sampleMovie = $testSetup->getSampleMovie();

        $this->containerMock = Mockery::mock(DependencyInjectionContainer::class);

        $this->listMovieCatalogTitleFilter = new ListMovieCatalogTitlesFilter($this->containerMock);
    }

    public function tearDown()
    {
        Mockery::close();
    }


    public function testCanCreate()
    {
        $this->assertNotNull($this->listMovieCatalogTitleFilter);
    }

    public function testWhenFilterIsInvalidReturnsAnErrorResponseInTheRequestWithFourSixsteenStatusCode()
    {
        $request = ($this->listMovieCatalogTitleFilter)(Request::createFromGlobals(
            Environment::mock(["REQUEST_METHOD" => "POST", "REQUEST_URI" => "/api/movies/title/searches"]))
            ->withParsedBody(["invalidFilter"]),
            new Response(),
            []);

        $this->assertNotNull($request->getAttribute('responseError'));
        $this->assertEquals(416, $request->getAttribute('responseError')->getStatusCode());
    }

    public function testWhenReuqestBodycontainsAValidFilterReturnsFilterAttributeInRequest()
    {
        $request = ($this->listMovieCatalogTitleFilter)(Request::createFromGlobals(
            Environment::mock(
                ["REQUEST_METHOD" => "POST",
                    "REQUEST_URI" => "/api/movies/title/searches"
                ])
        )
            ->withParsedBody(["filterBy" => "genre", "filterValue" => "ACTION"]),
            new Response(), []);

        $this->assertNotNull($request->getAttribute('filter'));
        $this->assertEquals(["filterBy" => "genre", "filterValue" => "ACTION"], $request->getAttribute('filter'));
    }


}
