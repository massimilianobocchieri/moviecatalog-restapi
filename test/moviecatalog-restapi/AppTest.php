<?php
declare(strict_types=1);

namespace MovieCatalogRestApi;

use Mockery;
use MovieCatalogRestApi\Infrastructure\Interfaces\Http\RouterInterface;
use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Doubles\InMemoryMovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;

class AppTest extends TestCase
{
    private $app;
    private $container;


    public function setUp()
    {
        $this->app = new App();

        $container = $this->app->getContainer();
        $container->set("movieCatalogService", function ($container) {
            return new MovieCatalogService($container, new InMemoryMovieCatalogRepository());
        });

        $this->container = $container;
    }

    public function testCanCreate()
    {
        $this->assertInstanceOf(App::class, $this->app);
    }

    public function testCanGetContainer()
    {
        $this->assertInstanceOf(DependencyInjectionContainer::class, $this->app->getContainer());
    }

    public function testCanRun()
    {
        $this->assertInstanceOf(ResponseInterface::class, $this->app->run(true));
    }

    public function testCanRespondAndFinalize()
    {
        $this->assertInstanceOf(ResponseInterface::class, $this->app->run());
    }

    public function testWhenItHasAnErrorWhileTryingToProcessResponseReturnsAResponseWithFiveHundredAndErrorMessage()
    {
        $this->container->set('router', function ($container) {
            return null;
        });

        $response = $this->app->run(true);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContains("Internal Server Error while processing you request", (string)$response->getBody());
    }


    public function testWhenItHasAnExceptionWhileTryingToProcessResponseReturnsAResponseWithFiveHundredAndErrorMessage()
    {
        $routerMock = Mockery::mock(RouterInterface::class);
        $routerMock->allows()->route(Mockery::any())
            ->andThrow(\Exception::class);
        $this->container->set('router',
            function ($container) use ($routerMock) {
            return $routerMock;
        });

        $response = $this->app->run(true);

        $this->assertEquals(500, $response->getStatusCode());
        $this->assertContains("Error while processing you request", (string)$response->getBody());
    }

}