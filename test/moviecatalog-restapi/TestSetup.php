<?php
declare(strict_types=1);

namespace MovieCatalogRestApi;

use MovieCatalogRestApi\Resources\Movies\Mappers\MovieJsonMapper;
use MovieCatalogRestApi\Resources\Movies\Model\Movie;
use MovieCatalogRestApi\Resources\Movies\Model\MovieCollection;
use Psr\Http\Message\ResponseInterface;

class TestSetup
{
    use MovieJsonMapper;

    private CONST SAMPLEMOVIEASARRAY = [
        "imdbid" => "tt0120587",
        "title" => "AntZ",
        "genre" => "ADVENTURE",
        "year" => 1998,
        "rating" => 7,
        "directors" => [
            ["lastName" => "Darnell", "firstName" => "Eric"],
            ["lastName" => "Johnson", "firstName" => "Tim"]
        ],
        "stars" => [
            ["lastName" => "Allen", "firstName" => "Wood"],
            ["lastName" => "Stone", "firstName" => "Sharon"],
            ["lastName" => "Hackman", "firstName" => "Gene"]

        ]];

    private CONST ANOTHERSAMPLEMOVIEASARRAY = [
        "imdbid" => "tt0112573",
        "title" => "Braveheart",
        "genre" => "DRAMA",
        "year" => 1995,
        "rating" => 8,
        "directors" => [
            ["lastName" => "Gibson", "firstName" => "Mel"]
        ],
        "stars" => [
            ["lastName" => "Gibson", "firstName" => "Mel"],
            ["lastName" => "Marceau", "firstName" => "Sophie"],
            ["lastName" => "Goohan", "firstName" => "Patrick"]

        ]];

    private CONST SAMPLEMOVIEUPDATEINFO = [
        "title" => "AntZ",
        "genre" => "ADVENTURE",
        "year" => 1998,
        "rating" => 7,
        "directors" => [
            ["lastName" => "Darnell", "firstName" => "Eric"],
            ["lastName" => "Johnson", "firstName" => "Tim"]
        ],
        "stars" => [
            ["lastName" => "Allen", "firstName" => "Wood"],
            ["lastName" => "Stone", "firstName" => "Sharon"],
            ["lastName" => "Hackman", "firstName" => "Gene"]

        ]];

    private CONST SAMPLEIMDBID = "tt1234567";

    public static function getSampleMovieAsArray(): array
    {
        return self::SAMPLEMOVIEASARRAY;
    }

    public static function getAnotherSampleMovieAsArray(): array
    {
        return self::ANOTHERSAMPLEMOVIEASARRAY;
    }

    public static function getSampleMoviesAsArray(): array
    {
        return [self::SAMPLEMOVIEASARRAY, self::ANOTHERSAMPLEMOVIEASARRAY];
    }

    public static function getMovieUpdateInfo(): array
    {
        return self::SAMPLEMOVIEUPDATEINFO;
    }

    public static function getSampleImDbId(): string
    {
        return self::SAMPLEIMDBID;
    }

    public function getSampleMovie(): Movie
    {
        return $this->movieFromJson(self::SAMPLEMOVIEASARRAY);
    }

    public function getAnotherSampleMovie(): Movie
    {
        return $this->movieFromJson(self::ANOTHERSAMPLEMOVIEASARRAY);
    }

    public function getSampleMovies(): MovieCollection
    {
        return new MovieCollection(
            $this->movieFromJson(self::SAMPLEMOVIEASARRAY),
            $this->movieFromJson(self::ANOTHERSAMPLEMOVIEASARRAY)
        );
    }

    public function getAnotherSampleMovies(): MovieCollection
    {
        return new MovieCollection(
            $this->movieFromJson(self::ANOTHERSAMPLEMOVIEASARRAY),
            $this->movieFromJson(self::SAMPLEMOVIEASARRAY)
        );
    }

    public static function formatResponseBodyAsArray(ResponseInterface $response): array
    {
        return json_decode((string)$response->getBody(), true);
    }

}