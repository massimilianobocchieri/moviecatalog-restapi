<?php
declare(strict_types=1);

namespace MovieCatalogRestApi;

use MovieCatalogRestApi\Infrastructure\Utilities\DependencyInjectionContainer;
use MovieCatalogRestApi\Resources\Movies\Doubles\InMemoryMovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Slim\Http\Environment;
use Slim\Http\Request;

class EndToEndIntegrationTest extends TestCase
{
    private $app;
    private $container;


    public function setUp()
    {
        $this->app = new App();

        $container = $this->app->getContainer();
        $container->set("movieCatalogService", function ($container) {
            return new MovieCatalogService($container, new InMemoryMovieCatalogRepository());
        });

        $this->container = $container;
    }

    public function testMoviesGetAll()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI' => '/api/movies',
        ]);

        $request = Request::createFromGlobals($environment);
        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();
        $this->assertSame($response->getStatusCode(), 200);
        //$this->assertSame((string)$response->getBody(), "Hello, Todo");
    }

    public function testMoviesGetTitlesByFilterGenreExistingReturnsOk()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies/title/searches',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(["filterBy" => "genre", "filterValue" => "ACTION"]);

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertSame($response->getStatusCode(), 200);
    }

    public function testMoviesGetTitlesByFilterGenreNotExistingReturnsFourOuFour()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies/title/searches',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(["filterBy" => "genre", "filterValue" => "DRAMA"]);

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertEquals($response->getStatusCode(), 200);
        $this->assertEquals([], TestSetup::formatResponseBodyAsArray($response));
    }

    public function testMoviesGetTitlesByFilterGenreInvalidReturnsFourSixOne()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies/title/searches',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(["filterBy" => "genre", "filterValue" => "DOESNTEXIST"]);

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertEquals($response->getStatusCode(), 416);
        $this->assertEquals(['validation error' => 'wrong genre value'], TestSetup::formatResponseBodyAsArray($response));
    }

    public function testMoviesCreateSingleMovieReturnsOk()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(TestSetup::getSampleMovieAsArray());

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertEquals($response->getStatusCode(), 201);
        $this->assertEquals(TestSetup::getSampleMovieAsArray(), TestSetup::formatResponseBodyAsArray($response));
    }

    public function testMoviesCreateMovieWithInvalidInfoReturnsFourSixOne()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(["invalid movie info"]);

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertEquals(416,$response->getStatusCode() );
        $this->assertArrayHasKey("validation error", TestSetup::formatResponseBodyAsArray($response));
    }

    public function testMoviesCreateMultipleMoviesReturnsOk()
    {
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI' => '/api/movies',
            'CONTENT_TYPE' => 'application/x-www-form-urlencoded'
        ]);
        $request = Request::createFromGlobals($environment)
            ->withParsedBody(TestSetup::getSampleMoviesAsArray());

        $this->injectRequestInContainer($request);

        $response = $this->getAppResponse();

        $this->assertEquals(201, $response->getStatusCode() );
        $this->assertEquals(TestSetup::getSampleMoviesAsArray(), TestSetup::formatResponseBodyAsArray($response));
    }


    //TODO To Be Continued for the remaining endpoint

    private function injectRequestInContainer(Request $request): void
    {
        $this->container->set('request', function ($container) use ($request) {
            return $request;
        });
    }

    private function getAppResponse(): ResponseInterface
    {
        return $this->app->run(true);
    }

}