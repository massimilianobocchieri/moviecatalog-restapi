<?php
declare(strict_types=1);

$loader = require __DIR__ . "/../vendor/autoload.php";

use MongoDB\Database;
use MongoDB\Driver\Manager;
use MovieCatalogRestApi\App;
use MovieCatalogRestApi\Resources\Movies\Repositories\MongoDbMovieCatalogRepository;
use MovieCatalogRestApi\Resources\Movies\Services\MovieCatalogService;

$app = new App();

$container = $app->getContainer();

//$container->set("movieCatalogService", function ($container) {
//    return new MovieCatalogService($container, new InMemoryMovieCatalogRepository(new MovieCollection(
//        (new MovieBuilder("tt1234567"))
//            ->title("a title")
//            ->genre(new Genre(Genre::ACTION))
//            ->year(2017)
//            ->rating(10)
//            ->directors(new PersonCollection(new Person("Nunu", "Peter Edward")))
//            ->stars(new PersonCollection(new Person("Nunu", "Dorina")))
//            ->build())));
//});

$container->set("movieCatalogService", function ($container) {

    $dbInfo = ($container->get('config'))->get('db');
    $manager    = new Manager("mongodb://{$dbInfo['host']}:{$dbInfo['port']}");
    $database   = new Database($manager, "db");
    $collection = $database->selectCollection('movies');

    return new MovieCatalogService($container, new MongoDbMovieCatalogRepository($collection) );
});


$app->run();
